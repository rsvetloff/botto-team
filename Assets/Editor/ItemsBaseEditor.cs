﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;

[CustomEditor(typeof(ItemsDataBase))]
public class ItemsBaseEditor : Editor
{
    private static bool addFold = false;
    private static ItemIndex addIndex = 0;
    private static string addDisplayName;
    private static string addDescription;
    private static ItemIndex addReplace;
    private static InventoryItemBase.InventoryItemTypes addType;
    private bool itemsFold = false;
    private Sprite addImage;
    private GameObject addPrefab;

    public override void OnInspectorGUI()
    {


        #region setup
        ItemsDataBase database = (ItemsDataBase)target;
        if (database.itemsData == null)
        {
            database.itemsData = new List<InventoryItemBase>();
        }
        List<InventoryItemBase> items = database.itemsData;
        
        if (database.inspectorCollapsed == null)
        {
            database.inspectorCollapsed = new List<bool>(items.Count);
        }
        List<bool> collaspeList = database.inspectorCollapsed;

        #endregion

        /*
        #region save load
        GUILayout.BeginHorizontal();
        if(GUILayout.Button("save"))
        {
            database.saveToFile();
        }
        if (GUILayout.Button("load"))
        {
            database.loadFromFile();
        }
        GUILayout.EndHorizontal();
        #endregion
    */
        #region Add Item
        GUILayout.BeginVertical("box");
        addFold = EditorGUILayout.Foldout(addFold, "Add Item");
        if (addFold)
        {
            addIndex = (ItemIndex)EditorGUILayout.EnumPopup("Item Code", addIndex);
            addDisplayName = EditorGUILayout.TextField("Display Name", addDisplayName);
            addImage = (Sprite)EditorGUILayout.ObjectField("UI Image", addImage, typeof(Sprite), false);
            addPrefab = (GameObject)EditorGUILayout.ObjectField("UI Image", addPrefab, typeof(GameObject), false);
            addReplace = (ItemIndex)EditorGUILayout.EnumPopup("Replace Item", addReplace);
            addType = (InventoryItemBase.InventoryItemTypes)EditorGUILayout.EnumPopup("Item Type", addType);
            EditorGUILayout.LabelField("Description");
            EditorGUILayout.BeginVertical("box");
            addDescription = EditorGUILayout.TextArea(addDescription);
            EditorGUILayout.EndVertical();
            if (GUILayout.Button("Add Item"))
            {
                InventoryItemBase newItem = new InventoryItemBase(addIndex, addDisplayName,addDescription, addImage, addPrefab);
                if (database.addItem(newItem))
                {
                    collaspeList.Add(false);
                }
            }
        }
        GUILayout.EndVertical();
        #endregion
        EditorGUILayout.Space();
        #region Editing
        itemsFold = EditorGUILayout.Foldout(itemsFold, "Items");
        if (itemsFold)
        {
            EditorGUILayout.BeginVertical("box");
            for (int i = 0; i < items.Count; i++)
            {
                collaspeList[i] = EditorGUILayout.Foldout(collaspeList[i], items[i].ToString());
                if (collaspeList[i])
                {
                    EditorGUILayout.BeginVertical();
                    EditorGUILayout.LabelField("Item Code", items[i]._itemCode.ToString());
                    items[i]._displayName = EditorGUILayout.TextField("Display Name", items[i]._displayName);
                    items[i]._prefab = (GameObject)EditorGUILayout.ObjectField("Prefab", items[i]._prefab, typeof(GameObject), false);
                    items[i]._UIImage = (Sprite)EditorGUILayout.ObjectField("Prefab", items[i]._UIImage, typeof(Sprite), false);
                    items[i]._replace = (ItemIndex)EditorGUILayout.EnumPopup("Replace Item", items[i]._replace);
                    items[i]._itemType = (InventoryItemBase.InventoryItemTypes)EditorGUILayout.EnumPopup("Item Type", items[i]._itemType);
                    EditorGUILayout.LabelField("Description");
                    EditorGUILayout.BeginVertical("box");
                    items[i]._description = EditorGUILayout.TextArea(items[i]._description);
                    EditorGUILayout.EndVertical();
                    if (GUILayout.Button("Remove"))
                    {
                        database.removeItem(items[i]);
                        collaspeList.RemoveAt(i);
                    }
                    EditorGUILayout.EndVertical();
                }
            }
            EditorGUILayout.EndHorizontal();
        }
        #endregion
    }

}
