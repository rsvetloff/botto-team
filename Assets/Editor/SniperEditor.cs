﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(Sniper))]
public class SniperEditor : Editor {

    

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        Sniper gun = (Sniper)target;
        SerializedObject particaleProperties;
        particaleProperties = new SerializedObject(gun.GetComponent<ParticleSystem>());
            //SerializedProperty it = particaleProperties.GetIterator();
            //while (it.Next(true))
            //    Debug.Log(it.propertyPath);     
        float particleSpeed = particaleProperties.FindProperty("InitialModule.startSpeed.scalar").floatValue;
        particaleProperties.FindProperty("InitialModule.startLifetime.scalar").floatValue = gun.range / particleSpeed;
        particaleProperties.ApplyModifiedProperties();
    }
}
