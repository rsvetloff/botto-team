﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(Shotgun))]
public class ShotgunEditor : Editor {

    

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        Shotgun gun = (Shotgun)target;
        SerializedObject particaleProperties;
        particaleProperties = new SerializedObject(gun.GetComponent<ParticleSystem>());
            //SerializedProperty it = particaleProperties.GetIterator();
            //while (it.Next(true))
            //    Debug.Log(it.propertyPath);     
        particaleProperties.FindProperty("ShapeModule.angle").floatValue =gun.coneAngle/2f;
        float particleSpeed = particaleProperties.FindProperty("InitialModule.startSpeed.scalar").floatValue;
        particaleProperties.FindProperty("InitialModule.startLifetime.scalar").floatValue = gun.range / particleSpeed;
        particaleProperties.ApplyModifiedProperties();
    }
}
