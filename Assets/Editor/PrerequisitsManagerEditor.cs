﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

[CustomEditor(typeof(itemPrerequisiteManager))]
public class PrerequisitsManagerEditor : Editor {



    private static bool addFold = false;
    private static ItemIndex addIndex = 0;
    private static ItemIndex prerequisiteIndex;
   // private ItemCollationType addCollate = 0;
    private static float addQuantity = 0f;
    private static bool addCollecable = false;

    private fileSaver<List<ItemPrerequisite>> saver;


   // private List<bool> foldout;
 
    public override void OnInspectorGUI()
    {
        #region Setup
        saver = new fileSaver<List<ItemPrerequisite>>("prerequisites");
        if(InventoryItem.datatbase == null)
        {
            InventoryItem.datatbase = GameObject.FindGameObjectWithTag(TagsIndex.GameManager).GetComponent<ItemsDataBase>();
        }
        itemPrerequisiteManager manager = (itemPrerequisiteManager)target;
        if(manager.prerequisites==null)
        {
            manager.prerequisites = new List<ItemPrerequisite>();
            Debug.Log("recreate preqs");
        }
        if (manager.foldout == null)
        {
            manager.foldout = new List<bool>(manager.Count);
        }

        #endregion

        #region save load
        GUILayout.BeginHorizontal();
        if (GUILayout.Button("save"))
        {
            saver.saveToFile(manager.prerequisites);
        }
        if (GUILayout.Button("load"))
        {
            List<ItemPrerequisite> loadedData = saver.loadFromFile();
            if(loadedData!=null)
            {
                manager.prerequisites = loadedData;
                manager.foldout = new List<bool>(new bool[loadedData.Count]);
            }
        }
        GUILayout.EndHorizontal();
        #endregion

        #region Add Item
        addFold = EditorGUILayout.Foldout(addFold, "Add Item");
        if (addFold)
        {
            EditorGUILayout.BeginVertical("box");
            EditorGUILayout.LabelField("Item");
            addIndex = (ItemIndex)EditorGUILayout.EnumPopup("Item Code", addIndex);
            EditorGUILayout.Space();
            EditorGUILayout.LabelField("Prerequisite");
            EditorGUILayout.Space();
            prerequisiteIndex = (ItemIndex)EditorGUILayout.EnumPopup("Prerequisite Item", prerequisiteIndex);
            //addCollate = (ItemCollationType)EditorGUILayout.EnumPopup("Item Collate", addCollate);
            addQuantity = EditorGUILayout.FloatField("Prerequisite Quantity", addQuantity);
            addCollecable = EditorGUILayout.Toggle("Filter By", addCollecable);
            if (GUILayout.Button("Add Prerequisite"))
            {
                manager.addPrerequiite(addIndex, prerequisiteIndex, addQuantity, addCollecable);
                Debug.Log("11");
                manager.foldout.Add(false);
                EditorUtility.SetDirty(manager);
            }
            EditorGUILayout.EndVertical();
        }
        #endregion

        #region Display Current
        for (int i = 0; i < manager.Count; i++)
        {
            #region setrup
            ItemPrerequisite pre = manager.getprerequisites()[i];
            Inventory reqs = pre.prerequisites;
           if(reqs.inspectorCollapsed==null)
            {
                reqs.inspectorCollapsed = new List<bool>(reqs.Count);
            }
           while(reqs.inspectorCollapsed.Count<reqs.Count)
            {
                reqs.inspectorCollapsed.Add(false);
            }
            #endregion
           manager. foldout[i] = EditorGUILayout.Foldout(manager.foldout[i], pre._displayName);
            if (manager.foldout[i])
            {
                EditorGUILayout.BeginVertical("box");
                List<bool> collaspeList = reqs.inspectorCollapsed;
                List<InventoryItem> items = reqs.getList();
                if (GUILayout.Button("Remove Item"))
                {
                    manager.removeItem(pre._itemCode);
                }
                for (int j = 0; j < reqs.Count; j++)
                {
                    collaspeList[j] = EditorGUILayout.Foldout(collaspeList[j], items[j].ToString());

                    if (collaspeList[j])
                    {
                        EditorGUILayout.BeginVertical("box");
                        EditorGUILayout.LabelField("Item Code", items[j]._itemCode.ToString());
                        EditorGUILayout.LabelField("Collation Type", items[j]._collationType.ToString());
                        items[j]._quantity = EditorGUILayout.FloatField("Quantity", items[j]._quantity);
                        items[j]._collectable = EditorGUILayout.Toggle("Filter By", items[j]._collectable);
                        if (GUILayout.Button("Remove"))
                        {
                            reqs.remove(items[j]);
                            reqs.inspectorCollapsed.RemoveAt(j);
                            EditorUtility.SetDirty(manager);
                        }
                        EditorGUILayout.EndVertical();
                    }
                }

                EditorGUILayout.EndVertical();
            }
        }

        #endregion


    }



}
