﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;
using UnityEngine.UI;

[CustomEditor(typeof(InventoryComponent))]
public class InventoryEditor : Editor
{
    private static  bool addFold = false;
    private static ItemIndex addIndex = 0;
    private static ItemCollationType addCollate = 0;
    private static float addQuantity = 0f;
    private static bool addCollecable = false;
    private  bool itemsFold = false;

    public override void OnInspectorGUI()
    {


        #region setup
        if (InventoryItem.datatbase == null)
        {
            InventoryItem.datatbase = GameObject.FindGameObjectWithTag(TagsIndex.GameManager).GetComponent<ItemsDataBase>();
        }
        InventoryComponent ic = (InventoryComponent)target;
        if (ic.inventory == null)
        {
            ic.inventory = new Inventory();
        }

        Inventory inventory = ic.getInventory();
       /*
        if (inventory._items == null)
        {
            inventory._items=new List<InventoryItem>();
            Debug.Log("Set Inventory LIst");
        }
        */
        List<InventoryItem> items = inventory.getList();
        if (inventory.inspectorCollapsed == null)
        {
            inventory.inspectorCollapsed = new List<bool>(items.Count);
        }
        List<bool> collaspeList = inventory.inspectorCollapsed;

        #endregion

        #region inventory properties

        ic.Collectable = EditorGUILayout.Toggle("Collectable", ic.Collectable);
        inventory.collectionBehavior = (InventoryBehaviors)EditorGUILayout.EnumPopup("Collection Behavior", inventory.collectionBehavior);
        #endregion

        #region Add Item
        if (GUILayout.Button("detach"))
        {

            
            Debug.Log("detached");
        }
        EditorUtility.SetDirty(ic);
        GUILayout.BeginVertical("box");
        addFold = EditorGUILayout.Foldout(addFold, "Add Item");
        if (addFold)
        {
            addIndex = (ItemIndex)EditorGUILayout.EnumPopup("Item Code", addIndex);
            addCollate = (ItemCollationType)EditorGUILayout.EnumPopup("Item Collate", addCollate);
            addQuantity = EditorGUILayout.FloatField("Quantity", addQuantity);
            addCollecable = EditorGUILayout.Toggle("Collecable", addCollecable);
            if (GUILayout.Button("Add Item"))
            {
                
                int pos = inventory.add(new InventoryItem(addIndex, addCollate, addQuantity, addCollecable));
                collaspeList.Insert(pos, false);
                //EditorUtility.SetDirty(ic);
            }
        }
        GUILayout.EndVertical();
        #endregion
        EditorGUILayout.Space();
        #region Editing
        itemsFold = EditorGUILayout.Foldout(itemsFold, "Items");
        if (itemsFold)
        {
            EditorGUILayout.BeginVertical("box");
            for (int i = 0; i < items.Count; i++)
            {
                collaspeList[i] = EditorGUILayout.Foldout(collaspeList[i], items[i].ToString());
                if (collaspeList[i])
                {
                    EditorGUILayout.BeginVertical();
                    EditorGUILayout.LabelField("Item Code", items[i]._itemCode.ToString());
                    EditorGUILayout.LabelField("Collation Type", items[i]._collationType.ToString());
                    items[i]._quantity = EditorGUILayout.FloatField("Quantity", items[i]._quantity);
                    items[i]._collectable = EditorGUILayout.Toggle("Collecable", items[i]._collectable);
                    if (GUILayout.Button("Remove"))
                    {
                        items.RemoveAt(i);
                        //EditorUtility.SetDirty(ic);
                    }
                    EditorGUILayout.EndVertical();
                }
            }
            EditorGUILayout.EndHorizontal();
        }
        #endregion
    }

}
