﻿using UnityEngine;
using System.Collections;

/// <summary>
/// controlls the main cammera.
/// follows a transform to keep it offset from screen center
/// </summary>
public class MainCameraController : MonoBehaviour {

	#region Automated Members


//	private Rigidbody playerBody;
	#endregion


	#region public members
	/// <summary>
	/// The camera's target transform.
	/// </summary>
	public Transform CameraTarget;
	[Range(0,1)]
	/// <summary>
	/// The camera move rate.
	/// </summary>
	public float cameraMoveRate;

	public float cameraRotateRate;
	/// <summary>
	/// The camera movment tollerance.
	/// </summary>
	public float cameraTollerance;

	/// <summary>
	/// The z position of the camera.
	/// this is the depth distance the cameraa maintains frfom the tracked object 
	/// this value will set to the current camera z position of start.
	/// </summary>
	public float zPosition;
	/// <summary>
	/// The X offset.
	/// the camera will keep its position on the x axis offset from the target transform by this amount
	/// </summary>
	public float XOffset;
	/// <summary>
	/// The Y offset.
	/// the camera will keep its position on the y axis offset from the target transform by this amount
	/// </summary>
	public float YOffset;
	#endregion


	#region Private Members

	#endregion

	// Use this for initialization
	void Start () 
	{
		#region Initilize automated members

	//	playerBody = CameraTarget.GetComponent<Rigidbody>();
		#endregion
		if (CameraTarget == null) 
		{
			CameraTarget = PlayerController.instance.cameraTarget;
		}
	
		zPosition = transform.position.z;

	}
	
	// Update is called once per frame
	void FixedUpdate () 
	{
        //Debug.Log (new Vector3 (CameraTarget.position.x, CameraTarget.position.y, zPosition).ToString ());
        //Vector3 newpos = Vector3.Lerp (transform.position, new Vector3 (CameraTarget.position.x, CameraTarget.position.y, zPosition), cameraMoveRate);
        //		Vector3 velocity = playerBody.velocity;
        //		transform.position = Vector3.SmoothDamp (transform.position, new Vector3 (CameraTarget.position.x, CameraTarget.position.y, zPosition),ref velocity, Time.deltaTime);
        lerpfollow();

    }
    

    /// <summary>
    /// follows and banks the camera twards the target transform.
    /// </summary>
	private void lerpfollow()
	{
        
        transform.position = Vector3.Lerp(transform.position, new Vector3(CameraTarget.position.x + XOffset, CameraTarget.position.y + YOffset, zPosition), cameraMoveRate);
        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(-transform.position + CameraTarget.position), cameraRotateRate);
        
	}
    /// <summary>
    /// ,odifies the offset of the camera from the target object.
    /// </summary>
    /// <param name="offset"></param>
    public void offsetCameraBy(Vector3 offset)
    {
        XOffset += offset.x;
        YOffset += offset.y;
        zPosition += offset.z;
    }

    public void offsetZ(float amount)
    {
        zPosition += amount;
    }
}
