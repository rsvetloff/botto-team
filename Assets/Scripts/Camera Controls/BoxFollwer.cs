﻿using UnityEngine;
using System.Collections;
/// <summary>
/// Box follwer.
/// implements a rectangle in screen space that moves to keep a transform in bounds 
/// </summary>
public class BoxFollwer : MonoBehaviour 
{


	#region Automated members
	/// <summary>
	/// The target.
	/// </summary>
	public Transform target;

	#endregion

	#region Public Memeber

	/// <summary>
	/// The width .
	public float width;

	/// <summary>
	/// The Height .
	/// </summary>
	public float height;

    /// <summary>
    /// the movment speed of the box
    /// </summary>
	public float moveSpeed;

	#endregion


	void Update()
	{

		if (target.position.x < transform.position.x - (width / 2)) 
		{
			transform.position -= Vector3.right * moveSpeed * Time.deltaTime;
		} 
		else if (target.position.x > transform.position.x + width / 2) 
		{
			transform.position += Vector3.right * moveSpeed * Time.deltaTime;
		}
		if (target.position.y < transform.position.y - (height / 2)) 
		{
			transform.position -= Vector3.up * moveSpeed * Time.deltaTime;
		} 
		else if (target.position.y > transform.position.y + height / 2) 
		{
			transform.position += Vector3.up * moveSpeed * Time.deltaTime;
		}
        ///debug 
		Debug.DrawLine (getTopLeft (), getTopRight ());
		Debug.DrawLine (getTopLeft (), getBottomLeft ());
		Debug.DrawLine (getBottoRight (), getBottomLeft ());
		Debug.DrawLine (getBottoRight (), getTopRight ());
	}


	private Vector3 getTopLeft()
	{
		return new Vector3 (transform.position.x - width / 2, transform.position.y + height / 2);
	}
	private Vector3 getTopRight()
	{
		return new Vector3 (transform.position.x + width / 2, transform.position.y + height / 2);
	}	
	private Vector3 getBottomLeft()
	{
		return new Vector3 (transform.position.x - width / 2, transform.position.y - height / 2);
	}
	private Vector2 getBottoRight()
	{
		return new Vector3(transform.position.x+width/2,transform.position.y-height/2);
	}
}
