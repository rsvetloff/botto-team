﻿using UnityEngine;
using System.Collections;

public abstract class HitEffect : MonoBehaviour
{

    public enum HitGroups
    {
        player,
        enemies
        
    }

    public HitEffectIndex _effectCode;

    public HitGroups group;

    public abstract void applyEffect(GameObject target);

}
