﻿using UnityEngine;
using System.Collections;
using System;
/// <summary>
/// a class to apply <seealso cref="GameAttribute"/> changes to a hit <seealso cref="GameAttributeManager"/>
/// </summary>
public class AttributeHitEffect : HitEffect
{
    /// <summary>
    /// the attribute o9f the target affected by the hit.
    /// </summary>
    public GameAttributeCodes affectedAttribute;
    /// <summary>
    /// the amount by which the attribute will be modified.
    /// </summary>
    public float hitValue;
    /// <summary>
    /// wil modify the attribute in a target object matching this effects' code by the hitValue
    /// </summary>
    /// <param name="target">the target to modify attributes for.</param>
    public override void applyEffect(GameObject target)
    {
        GameAttributeManager targetAttributes = target.GetComponentInParent<GameAttributeManager>();
        if(targetAttributes!=null)
        {
            targetAttributes.modifiyAttributeValue(affectedAttribute, hitValue);
        }
    }
}
