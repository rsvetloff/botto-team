﻿using UnityEngine;
using System.Collections;
using System;
/// <summary>
/// responds to hits from particle systems.
/// </summary>
public class PaticalHitResponder : HitResponder
{
    int lastHitID = 0;

    public HitEffect.HitGroups group;

    public Rigidbody _rbody;

    void OnParticleCollision(GameObject other)
    {
        Weapon weapon = other.GetComponent<Weapon>();
        if(weapon!=null)
        {
            applyWeaponHit(weapon);
        }
        else
        {
            applyHit(other);
        }
    }
    /// <summary>
    /// applies the hit effects on the game object to other
    /// </summary>
    /// <param name="other">the object to apply effects to.</param>
    private void applyHit(GameObject other)
    {
        HitEffect[] effects = other.GetComponents<HitEffect>();
        for(int i=0;i<effects.Length;i++)
        {
            if (effects[i].group == group)
            {
                effects[i].applyEffect(gameObject);
            }
        }
    }
    /// <summary>
    /// applies the hit efect of a weapon particle.
    /// </summary>
    /// <param name="weapon"></param>
    private void applyWeaponHit(Weapon weapon)
    {
        if (weapon != null && lastHitID != weapon.ShotID) //tests that the particle hit is not from a shot previouslly hit.
        {
            applyHit(weapon.gameObject);
            if(_rbody!=null)
            {
                _rbody.AddForce(weapon.LastShotDirection * weapon.recoilForce);
            }
            lastHitID = weapon.ShotID;
        }
    }
}
