﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
/// <summary>
/// this class fades ui images in and uot by modifing their alpha valus
/// </summary>
public class imageFader : MonoBehaviour {

    /// <summary>
    /// the image affected by the fader
    /// </summary>
    public Image affectedImage;
    /// <summary>
    /// the fading lerp factor.
    /// </summary>
    public float fadeDuration;
    /// <summary>
    /// the number of seconds that the fader will wait before befining the fade.
    /// </summary>
    public float initialDelay;
    
    private bool isFading=false;
    /// <summary>
    /// indicates that the fader is currentlly running.
    /// </summary>
    public bool IsFading 
    {
        get
        {
            return isFading;
        }
    }
    /// <summary>
    /// the tollerance under whitch the fading poroccess stops.
    /// </summary>
    [Range(0f,1f)]
    public float fadeTollerance;
    /// <summary>
    /// controlls if fading in or out.
    /// </summary>
    private int direction;
    /// <summary>
    /// the initial alpha value of fade start.
    /// </summary>
    private float initialAlpha;
    /// <summary>
    /// the color used to modify the image
    /// </summary>
    private Color modifingColor;
    /// <summary>
    /// indicates if the affected image will be disabled after fading is complete.
    /// </summary>
    private bool _disableOnComplition;

    private float fadeStartTime;

    void Awake()
    {
        this.enabled = false;
    }
	// Update is called once per frame
	void Update ()
    {
	    if(isFading && Time.time>initialDelay+fadeStartTime)
        {
            float totalTime = fadeStartTime + fadeDuration + initialDelay;
            modifingColor.a = Mathf.Lerp(initialAlpha, direction, (Time.time-(fadeStartTime+initialDelay))/(fadeDuration)) ;
            if(Mathf.Abs( Time.time-(totalTime))<fadeTollerance || Time.time>totalTime)
            {
                modifingColor.a = direction;
                isFading = false;
                this.enabled = false;
                if(_disableOnComplition)
                {
                    affectedImage.gameObject.SetActive(false);
                }
            }
            affectedImage.color = modifingColor;
            //Debug.Log(affectedImage.color.a);
        }
	}


    public void fadeout(bool disableOnComplition)
    {

        direction = 0;
        beginFade(disableOnComplition);
       
    }


    public void fadein(bool disableOnComplition)
    {
        direction = 1;
        beginFade(disableOnComplition);
    
    }

    private void beginFade(bool disableOnComplition)
    {
        modifingColor = affectedImage.color;
        affectedImage.gameObject.SetActive(true);
        isFading = true;
        this.enabled = true;
        _disableOnComplition= disableOnComplition;
        fadeStartTime = Time.time;
        initialAlpha = affectedImage.color.a;
    }
}
