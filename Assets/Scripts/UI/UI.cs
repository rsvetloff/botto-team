﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class UI : MonoBehaviour {
    /// <summary>
    /// reffrence
    /// </summary>
    public static UI instance;
    /// <summary>
    /// master text object
    /// </summary>
    public Text masterText;
    /// <summary>
    /// the time a message displayes before being removed.
    /// </summary>
    public int displayTime;
    /// <summary>
    /// mesegges list
    /// </summary>
    private List<string> displayTexts;
    /// <summary>
    /// timmer list
    /// </summary>
    private List<float> displaytimmers;

    public  const string CR = "\n";

    void Awake()
    {
        instance = this;
        displayTexts = new List<string>();
        displaytimmers = new List<float>();
    }

	public void addMessage(string message,float delay)
    {
        displayTexts.Add(message);
        displaytimmers.Add(Time.time + delay);

    }

    private void createMainText()
    {
        string result = "";
        int pointer = 0;
        while (pointer<displayTexts.Count)
        {
            if(displaytimmers[pointer]<Time.time) //delete message
            {
                displayTexts.RemoveAt(pointer);
                displaytimmers.RemoveAt(pointer);
            }
            else
            {
                if(result!="")
                {
                    result += CR;
                }
                result += displayTexts[pointer];
                pointer++;
            }
        }
        masterText.text = result;
    }

    private void FixedUpdate()
    {
        createMainText();
    }
}
