﻿using UnityEngine;
using System.Collections;

public class TutorialPageManager : MonoBehaviour
{
    
    public float displayTimer;

    public bool pageEnabled=true;

    private float startTime;

	
    private void Update()
    {
        if(Time.time>startTime + displayTimer)
        {
            hide();
        }
    }

    public void show()
    {
        if (pageEnabled && !gameObject.activeSelf)
        {
            
            cleanWraper();
            RectTransform newPageRT = GetComponent<RectTransform>();
            newPageRT.SetParent(GameManager.instance.tutorialScreen);
            newPageRT.offsetMax = Vector2.zero;
            newPageRT.offsetMin = Vector2.zero;
            GameManager.instance.tutorialScreen.gameObject.SetActive(true);
            gameObject.SetActive(true);
            enabled = true;
            startTime = Time.time;
        }
    }

    private void hide()
    {
        pageEnabled = false;
        enabled = false;
        GameManager.instance.tutorialScreen.gameObject.SetActive(false);
        gameObject.SetActive(false);
        //Destroy(GameManager.instance.tutorialScreen.GetChild(0).gameObject);
    }

    private void cleanWraper()
    {
        TutorialPageManager[] pages = GameManager.instance.tutorialScreen.GetComponentsInChildren<TutorialPageManager>();
        for(int i=0;i<pages.Length;i++)
        {
            if(pages[i].gameObject.activeSelf)
            {
                pages[i].hide();
            }
        }
    }
}
