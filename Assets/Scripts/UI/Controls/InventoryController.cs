﻿using UnityEngine;
using System.Collections;

public abstract class InventoryController : Controller
{

    
    

    /// <summary>
    /// the filter types used to generate the active set from the full set.
    /// </summary>
    public enum SelectorGenerationType
    {
        /// <summary>
        /// an empty filter.
        /// </summary>
        ALL = 0,
        /// <summary>
        /// uses a <seealso cref="itemPrerequisiteManager"/> to filter.
        /// </summary>
        PREREQUISITE = 1,
        /// <summary>
        /// filtrs by items that existing the players inventory
        /// </summary>
        EXISTS = 2
    }

    /// <summary>
    /// the currentlly active set of the selector
    /// </summary>
    protected Inventory activeSet;
    /// <summary>
    /// the complete set of items available to the selector.
    /// </summary>
    protected InventoryComponent fullSet;
    /// <summary>
    /// the plyers current inventory
    /// </summary>
    protected InventoryComponent playerInventory;
    /// <summary>
    /// the currently selected item.
    /// </summary>
    protected ItemPrerequisite currentSelection;
    

    public itemPrerequisiteManager prerequisites;
    /// <summary>
    /// the type of filtering used when building the active set.
    /// </summary>
    public SelectorGenerationType generationType;
    /// <summary>
    /// Indicates if the qactive set will always contain a null.
    /// </summary>
    public bool forceNull;

    protected InventoryItem selection;

    public ItemPrerequisite CurrentSelection
    {
        get
        {
            return currentSelection;
        }
    }

    

    protected virtual void Awake()
    {
        fullSet = GetComponent<InventoryComponent>();
        fullSet.sortInventoryByItemName();
        GameObject player = GameObject.FindGameObjectWithTag(TagsIndex.Player.ToString());
        playerInventory = player.GetComponentInParent<InventoryComponent>();
        prerequisites = GameManager.instance.GetComponent<itemPrerequisiteManager>();
        initilize();
    }

    public override void buildSelector()
    {

        generateActiveSet();
        if ((!forceNull) && activeSet.Count > 0)
        {
            setCurrentSelection(0);
        }
        //setImage();
        //setButtons();
    }

    
    /// <summary>
    /// generates the active set from the full set using the selected filter.
    /// </summary>
    protected override void generateActiveSet()
    {
        activeSet = new Inventory();
        switch (generationType)
        {
            case SelectorGenerationType.ALL:
                activeSet = generateByAll();
                break;
            case SelectorGenerationType.EXISTS:
                activeSet = generateByExists();
                break;
            case SelectorGenerationType.PREREQUISITE:
                activeSet = generateByPrerequisite();
                break;
        }
    }

    /// <summary>
    /// generates an inventory using the all filter
    /// </summary>
    /// <returns></returns>
    protected Inventory generateByAll()
    {
        return fullSet.inventory;

    }

    /// <summary>
    /// generates an inventory using the prerequisites filter
    /// </summary>
    /// <returns></returns>
    protected Inventory generateByPrerequisite()
    {
        Inventory result = new Inventory();
        for (int i = 0; i < fullSet.Count; i++)
        {
            if (playerInventory.checkItemPrerequisite(prerequisites.getPrereauisite(fullSet.lookAt(i))))
            {
                result._items.Add(fullSet.lookAt(i));
            }


        }
        return result;
    }

    /// <summary>
    /// generates an inventory using the exists filter.
    /// </summary>
    /// <returns></returns>
    protected Inventory generateByExists()
    {
        Inventory result = new Inventory();
        for (int i = 0; i < fullSet.Count; i++)
        {
            InventoryItem item = fullSet.lookAt(i);
            if (playerInventory.indexOf(item) > -1)
            {
                result._items.Add(item);
            }
        }
        return result;
    }

    protected abstract void setImage();
    
    /// <summary>
    /// sets the current selection in the controller
    /// </summary>
    /// <param name="index"></param>
    public override void setCurrentSelection(int index)
    {
        base.setCurrentSelection(index);
        currentSelectionIndex = index;
        if (index > -1)
        {
            selection = activeSet.lookAt(currentSelectionIndex);
            currentSelection = prerequisites.getPrereauisite(selection);
        }
        else
        {
            selection = null;
            currentSelection = null;
        }

        setDescriptionText();
        setImage();
        setButtons();
        invokeValueChanged(currentSelectionIndex);
    }
    /// <summary>
    /// setgs the current selected item in the selector.
    /// </summary>
    /// <param name="itemIndex">the itemindex to select</param>
    /// <param name="forceChange">indicates if to force a chnage even if the item is not in the selector active set</param>
    public virtual void setCurrentSelection(ItemIndex itemIndex, bool forceChange)
    {
        int index = activeSet.findItemIndex(itemIndex);
        if (index > -1)
        {
            setCurrentSelection(index);
        }
        else if (forceChange)
        {
            resetSelection();
        }
    }


    protected void resetSelection()
    {
        if (currentSelectionIndex != -1 && currentSelection != null)
        {
            int index = activeSet.findItemIndex(currentSelection._itemCode);
            if (index > -1)
            {
                setCurrentSelection(index);
            }
            else
            {
                int replaceIndex = activeSet.findItemIndex(currentSelection._replace);
                if (replaceIndex != -1)
                {
                    setCurrentSelection(replaceIndex);
                }
                else
                {
                    setCurrentSelection(-1);
                }

            }


        }

    }
    protected abstract void setDescriptionText();

    protected abstract void setButtons();


}
