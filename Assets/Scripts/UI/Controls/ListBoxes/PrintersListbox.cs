﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

[RequireComponent(typeof(RectTransform))]
public class PrintersListbox : Controller
{

    /// <summary>
    /// a prefab for a button in the list.
    /// </summary>
    public GameObject ListButton;
    /// <summary>
    /// the parent objet for the item buttons of the list.
    /// </summary>
    public RectTransform ListWraper;
    /// <summary>
    /// a prefab for the list scroll bar.
    /// </summary>
    public Scrollbar scrollBar;
    /// <summary>
    /// the currentlly selected printer;
    /// </summary>
    public PrinterTriggerScript currentSelection;

    private RawImage printerDisplay;


    public override void buildSelector()
    {
        generateActiveSet();

    }



    public override void initilize()
    {
        buildSelector();
        setCurrentSelection(LevelManager.ActiveLevel.ActivePrinterIndex);
    }

    protected override void generateActiveSet()
    {

        fillListWraper();
    }

    public override void setCurrentSelection(int index)
    {
        base.setCurrentSelection(index);
        LevelManager currentLevel = LevelManager.ActiveLevel;
        
        if(currentLevel.printerStatuses[index])
        {
            currentLevel.printers[currentSelectionIndex].uiCamera.gameObject.SetActive(false);
            currentSelectionIndex = index;
            currentLevel.printers[currentSelectionIndex].uiCamera.gameObject.SetActive(true);
            currentSelection = currentLevel.printers[index];
            invokeValueChanged(index);
        }
    }

    

    public void CleanListWraper()
    {
        ListboxButton[] buttons = ListWraper.GetComponentsInChildren<ListboxButton>();

        for (int i = 0; i < buttons.Length; i++)
        {
            buttons[i].unRegister();
            Destroy(buttons[i].gameObject);
        }
    }

    public void fillListWraper()
    {
        LevelManager currentLevel = LevelManager.ActiveLevel;
        CleanListWraper();
        int vOffset = 0;
        RectTransform wraper = ListWraper.GetComponent<RectTransform>();
        int activePrinterCount=0;
        for (int i = 0; i < currentLevel.printerStatuses.Length; i++)
        {
            if (currentLevel.printerStatuses[i])
            {
                activePrinterCount++;
            }
        }
        wraper.sizeDelta = new Vector2(wraper.sizeDelta.x, ListButton.GetComponent<RectTransform>().sizeDelta.y * activePrinterCount);
        for (int i = 0; i < currentLevel.printers.Length; i++)
        {
            if (currentLevel.printerStatuses[i])
            {
                GameObject newButton = Instantiate(ListButton);



                newButton.transform.SetParent(ListWraper.transform);
                RectTransform rtransform = newButton.GetComponent<RectTransform>();
                rtransform.anchorMax = new Vector2(((RectTransform)scrollBar.transform).anchorMin.x,rtransform.anchorMax.y);
                rtransform.offsetMax = new Vector2(0, rtransform.sizeDelta.y);
                rtransform.offsetMin = Vector2.zero;
                rtransform.anchoredPosition = new Vector2(0, rtransform.sizeDelta.y * -vOffset - rtransform.sizeDelta.y / 2);
                rtransform.offsetMax = new Vector2(-ListWraper.offsetMax.x, rtransform.offsetMax.y);
                vOffset++;


                ListboxButton listButton = newButton.GetComponent<ListboxButton>();
                listButton.setText(currentLevel.printers[i].printerName);
                listButton.ValueIndex = i;
            }
            //GetComponentInParent<ScrollRect>().verticalNormalizedPosition = 1f;
        }
    }
}
