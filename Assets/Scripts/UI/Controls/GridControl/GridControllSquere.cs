﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(RectTransform))]
public class GridControllSquere : MonoBehaviour {

    
    /// <summary>
    /// the <seealso cref="InventoryItem"/> belonging to the current box
    /// </summary>
    private InventoryItem _item;
    public InventoryItem Item
    {
        set
        {
            initilize(value);
        }
    }
    /// <summary>
    /// the image component that contains the boxs' visual image.
    /// </summary>
    public Image _displayImage;
    /// <summary>
    /// the last instance created of this class.
    /// </summary>
    public static GridControllSquere lastInstance;
    /// <summary>
    /// the sub menu calls =when a box is activated.
    /// </summary>
    public GameObject subMenuPrefab;
    /// <summary>
    /// the <seealso cref="InventoryController"/> who manages this box.
    /// </summary>
    public InventoryGridController controller;
    /// <summary>
    /// the masking object that indicates the box is selected.
    /// </summary>
    public GameObject selectedMask;

    void Awake()
    {
        lastInstance = this;
        controller = GetComponentInParent<InventoryGridController>();
        controller.OnValueChange += OnValueChanged;
    }
    /// <summary>
    /// initilizes the box with a given item.
    /// </summary>
    /// <param name="item">the item to initilize the box by</param>
    private void initilize(InventoryItem item)
    {
        _item = item;
        _displayImage.overrideSprite = _item._UIImage;
    }

    public void boxedClicked()
    {
        // subMenuInstantiate();
        controller.setCurrentSelection(_item, true);

    }
    /// <summary>
    /// instantiates and mositions the sub menu.
    /// </summary>
    private void subMenuInstantiate()
    {
        GameObject newBox = Instantiate(subMenuPrefab);
        RectTransform newBoxTransform = (RectTransform)newBox.transform;
        Vector2 newboxDelta = newBoxTransform.sizeDelta;
        newBoxTransform.SetParent(transform);
        newBoxTransform.sizeDelta = newboxDelta;
    }

    private void OnValueChanged(int index)
    {
        InventoryItem item = controller.CurrentSelection;
        selectedMask.SetActive(item!=null && item._itemCode == _item._itemCode);
    } 

    public void removeBox()
    {
        controller.OnValueChange -= OnValueChanged;
        Destroy(gameObject);
    }
}
