﻿using UnityEngine;
using System.Collections;
using System;

public class InventoryGridController : InventoryController
{


    /// <summary>
    /// the parent transfoerm of the grid boxes
    /// </summary>
    public RectTransform gridWraper;
    /// <summary>
    /// indicates if the boxes should be deployed vertically
    /// </summary>
    //public bool verticalGrid;
    /// <summary>
    /// the subMenu
    /// </summary>
    private craftingSubMenuController subMenu;
    /// <summary>
    /// the prefab for the grid bub menu.
    /// </summary>
    public GameObject subMenuPrefab;
    /// <summary>
    /// the number of boxes deployed per row.
    /// </summary>
    public int boxesPerRow;

    public RectTransform infoSection;

    private CraftingMenuManager menuManager;

    /// <summary>
    /// the prefab oif the sub menu
    /// </summary>
    public GameObject boxPrefab;

    protected override void Awake()
    {
        
        GameObject subMenuObject = Instantiate(subMenuPrefab);
        subMenuObject.transform.SetParent(infoSection);
        resetToAnchores((RectTransform)subMenuObject.transform);
        subMenu = subMenuObject.GetComponent<craftingSubMenuController>();
        menuManager = GetComponentInParent<CraftingMenuManager>();
        base.Awake();
    }
    public override void initilize()
    {
        generateActiveSet();
        deployBoxes();
        
    }

    protected override void setButtons()
    {
        
    }

    protected override void setDescriptionText()
    {
        
    }

    protected override void setImage()
    {
        
    }

    public override void buildSelector()
    {
        generateActiveSet();
        deployBoxes();
        setCurrentSelection(-1);
        //base.buildSelector();
        
    }

    private void deployBoxes()
    {
        clearWraper();
        float boxAnchor = 1f / boxesPerRow;
        int rowNumber = 0;
        for(int i=boxesPerRow;i<activeSet.Count+boxesPerRow;i++)
        {
            GameObject newBox = Instantiate(boxPrefab);
            RectTransform newBoxTransform = (RectTransform) newBox.transform;
            newBoxTransform.SetParent(gridWraper);
            newBoxTransform.anchorMax = new Vector2(boxAnchor *(i%boxesPerRow +1),1- (rowNumber)*boxAnchor);
            newBoxTransform.anchorMin = new Vector2(boxAnchor * (i%boxesPerRow) ,1- (rowNumber+1)*boxAnchor);
            resetToAnchores(newBoxTransform);
            newBox.SetActive(true);
            GridControllSquere.lastInstance.Item=activeSet.lookAt(i-boxesPerRow);
            GridControllSquere.lastInstance.controller = this;
            
            if((i + 1)%boxesPerRow ==0)
            {
                rowNumber++;
            }
           
        }
        subMenu.show();
    }

    public  void setCurrentSelection(InventoryItem item, bool forceChange)//,RectTransform selectedBox)
    {
        setCurrentSelection(item._itemCode, forceChange);
        if(currentSelection!=null)
        {
            menuManager.updateCost(currentSelection);
        }
        //selectedBox. SetSiblingIndex( gridWraper.childCount-1);
        subMenu.initilizeItem( item);
    }

    private void resetToAnchores(RectTransform target)
    {
        target.anchoredPosition = Vector2.zero;
        target.offsetMax = Vector2.zero;
        target.offsetMin = Vector2.zero;
        target.sizeDelta = Vector2.zero;
    }

    private void clearWraper()
    {
        GridControllSquere[] boxes = gridWraper.GetComponentsInChildren<GridControllSquere>();
        for (int i=0;i<boxes.Length;i++)
        {
            boxes[i].removeBox();
        }
    }
}
