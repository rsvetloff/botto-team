﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(RectTransform))]
public class craftingSubMenuController : MonoBehaviour {
    /// <summary>
    /// the prerequisite contained by the submenu
    /// </summary>
    private ItemPrerequisite _item;
    /// <summary>
    /// the item description text component.
    /// </summary>
    public Text descriptionText;
    /// <summary>
    /// the data cost text component
    /// </summary>
    public Text costText;
    /// <summary>
    /// the craft button text component.
    /// </summary>
    public Text buttonText;
    /// <summary>
    /// the crafy=t action button;
    /// </summary>
    public Button craftButton;
    /// <summary>
    /// the <seealso cref="CraftingMenuManager"/> controlling this menu;
    /// </summary>
    CraftingMenuManager menuManager;
    /// <summary>
    /// the collor of the crafting button when craft is applied.
    /// </summary>
    public Color buttonCraftColor;
    /// <summary>
    /// the collor of the crafting button when ungrade is applied.
    /// </summary>
    public Color buttonUpradeColor;
    

    void Awake()
    {
        menuManager = GetComponentInParent<CraftingMenuManager>();
    }

	public void show()
    {
        gameObject.SetActive(true);
        //initilizeLocation(location);
        
        initilizeItem(null);
        correctTetSize(descriptionText.rectTransform);
        correctTetSize(buttonText.rectTransform);
    }
    /// <summary>
    /// initilizes a new item in the menu.
    /// </summary>
    public void initilizeItem(InventoryItem item)
    {
        if (item != null)
        {
            _item = GameManager.instance.GetComponent<itemPrerequisiteManager>().getPrereauisite(item);
        }
        else
        {
            _item = null;
        }
        if (_item == null)
        {
            descriptionText.text = "";
            costText.text = menuManager.costPrefix;
            //correctTetSize(costText.rectTransform);
            buttonText.text = menuManager.CraftText;
            craftButton.interactable = false;
            
        }
        else
        {
            descriptionText.text = _item._description;
            float cost =  _item.getPrerequisite(ItemIndex.Data_Normal)._quantity;
            costText.text = menuManager.costPrefix + cost ;
            craftButton.interactable = cost <= menuManager.currentData;
          
            //correctTetSize(costText.rectTransform);
            if(_item._replace == ItemIndex.EmptyItem)
            {
                buttonText.text = menuManager.CraftText;
                ColorBlock colors = craftButton.colors;
                colors.normalColor = buttonCraftColor;
                craftButton.colors = colors;
            }
            else
            {
                buttonText.text = menuManager.UpgradeText;
                ColorBlock colors = craftButton.colors;
                colors.normalColor = buttonUpradeColor;
                craftButton.colors = colors;
            }
            
        }
    }
    /// <summary>
    /// mioves and sizes the menu to a given position and size.
    /// </summary>
    /// <param name="location"></param>
    private void initilizeLocation(RectTransform location)
    {
        RectTransform thisTransform = (RectTransform)transform;
        thisTransform.SetParent(location);
        thisTransform.anchoredPosition = Vector2.zero;
        thisTransform.offsetMax = Vector2.zero;
        thisTransform.offsetMin = Vector2.zero; 
        
    }

    private void correctTetSize(RectTransform target)
    {
        /*
        RectTransform parentTransform =(RectTransform) target.parent;
        Vector2 parentRatios = target.anchorMax - target.anchorMin;
        target.offsetMax= new Vector2(parentTransform.rect.width * parentRatios.x *0.5f  , parentTransform.rect.height * parentRatios.y*0.5f);
        target.offsetMin = -target.offsetMax;
        */
        target.offsetMax = Vector2.zero;
        target.offsetMin = Vector2.zero;
        target.offsetMax = new Vector2(target.rect.width*(1/target.lossyScale.x)*0.5f, target.rect.height * (1 / target.lossyScale.x)*0.5f);
        target.offsetMin = -target.offsetMax;
        //target.sizeDelta = Vector2.zero;
        //target.offsetMax = Vector2.zero;
        //target.offsetMin = Vector2.zero;
    }

    public void craft()
    {
        menuManager.craft();
    }
}
