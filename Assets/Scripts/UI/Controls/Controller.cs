﻿using UnityEngine;
using System.Collections;

public abstract class Controller : MonoBehaviour
{
    public delegate void _valueChange(int selectionIndex);
    public event _valueChange OnValueChange;
    //public delegate void ValueChange(int index);
    //public event ValueChange OnValueChange;

    public AudioSource clicker;
    /// <summary>
    /// the index in the active set of the current selected value
    /// </summary>
    protected int currentSelectionIndex;

    public int CurrentSelectionIndex
    {
        get { return CurrentSelectionIndex; }
    }

    public abstract void buildSelector();

    public abstract void initilize();

    protected abstract void generateActiveSet();

    public virtual void setCurrentSelection(int index)
    {
        if(clicker!=null)
        {
            clicker.Play();
        }
    }

    protected void invokeValueChanged(int itemIndex)
    {
        if (OnValueChange != null)
        {
            OnValueChange.Invoke(itemIndex);
        }
    }
}
