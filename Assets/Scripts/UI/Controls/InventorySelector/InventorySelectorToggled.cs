﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(InventoryComponent))]
[RequireComponent(typeof(RectTransform))]
public  class InventorySelectorToggled : InventoryController {

    

    #region inspector members

   
    public Text descriptionText;

    /// <summary>
    /// the object instantiated in to the selector.
    /// </summary>
    public GameObject selectorItem;

    public Image selectedItemImage;

    public Button upButton;
    public Button downButton;

    public bool onEnter;
    #endregion






    public override void initilize()
    {
        
        buildSelector();
        resetSelection();
        setButtons();
        setImage();
        
    }

    public void initilize (ItemIndex code)
    {
        initilize();
        setCurrentSelection(code, false);
        
    }

    protected override void setImage()
    {
        if (selection!=null)
        {
            selectedItemImage.overrideSprite = selection._UIImage;
        }
        else
        {
            selectedItemImage.overrideSprite = null;
        }
    }

    

    protected override void setButtons()
    {
        if (activeSet == null || activeSet.Count==0)
        {
            upButton.interactable = false;
            downButton.interactable = false;
        }
        else
        {
            if (currentSelectionIndex >= activeSet.Count - 1)
            {
                upButton.interactable = false;
            }
            else
            {
                upButton.interactable = true;
            }
            if (currentSelectionIndex <= 0)
            {
                downButton.interactable = false;
            }
            else
            {
                downButton.interactable = true;
            }
        }
    }

    protected override   void setDescriptionText()
    {
        if (selection == null)
        {
            descriptionText.text = "";
        }
        else
        {
            descriptionText.text = selection._description;
        }
    }

    public void stepUp()
    {
       if(currentSelectionIndex<activeSet.Count-1)
        {
            currentSelectionIndex++;
            setCurrentSelection(currentSelectionIndex);
        }
    }

    public void stepDown()
    {
        if (currentSelectionIndex >0)
        {
            currentSelectionIndex--;
            setCurrentSelection(currentSelectionIndex);
        }
    }

    public void OnMouseEnter()
    {
        if (!descriptionText.text.Equals(""))
        {
            descriptionText.gameObject.SetActive(onEnter);
        }

    }

    public void OnMouseExit()
    {
        if (!descriptionText.text.Equals(""))
        {
           descriptionText.gameObject.gameObject.SetActive(!onEnter);
        }

    }

}
