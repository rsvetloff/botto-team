﻿using UnityEngine;
using System.Collections;

public class TutorialTrigger : MonoBehaviour {

    public TutorialPageManager page;

    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log("printer Triggered");
        PlayerController player = other.gameObject.GetComponentInParent<PlayerController>();
        //Debug.Log(player.name);
        if (player != null && page != null)
        {
            page.show();
        }
    }
}
