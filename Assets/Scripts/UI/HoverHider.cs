﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(RectTransform))]
public class HoverHider : MonoBehaviour
{
    public GameObject target;
    public bool onEnter;
    
    public void OnMouseEnter()
    {
        target.SetActive(onEnter);
      
    }

    public void OnMouseExit()
    {
        target.SetActive(!onEnter);
        
    }
}
