﻿using UnityEngine;
using System.Collections;
using System;

public class PrinterMenuManager : MenuController
{

   
    public override void display()
    {
        Time.timeScale = 0f;
        GameManager.instance._lowPause = true;
        target.SetActive(true);
    }

    public override void hide()
    {
        target.SetActive(false);
        
    }

    public void resumGame()
    {
        hide();
        Time.timeScale = 1f;
        GameManager.instance._lowPause = false;
    }

}
