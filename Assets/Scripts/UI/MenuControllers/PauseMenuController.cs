﻿using UnityEngine;
using System.Collections;
using System;

public class PauseMenuController : MenuController
{
    public GameObject OptionsMenuPrefab;

    private OptionsMenuController optionsMenu;

    

    void Start()
    {
        GameObject options = Instantiate(OptionsMenuPrefab);
        RectTransform opotionsRT = options.GetComponent<RectTransform>();

        opotionsRT.SetParent(target.transform);
        opotionsRT.offsetMax = Vector2.zero;
        opotionsRT.offsetMin = Vector2.zero;
        optionsMenu = options.GetComponent<OptionsMenuController>();
        options.SetActive(false);

        target.SetActive(false);
    }

    void Update()
    {
        if(Input.GetKey(KeyCode.Escape))
        {
            display();
        }
    }

    public void exitGame()
    {
        Application.Quit();
    }

    public override void display()
    {
        GameManager.instance._lowPause = true;
        target.gameObject.SetActive(true);
    }

    public override void hide()
    {
        target.SetActive(false);
        GameManager.instance._lowPause = false;
    }

    public void showOptionsMenu()
    {
        optionsMenu.gameObject.SetActive(true);
    }
}
