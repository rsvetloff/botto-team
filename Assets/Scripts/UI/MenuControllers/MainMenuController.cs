﻿using UnityEngine;
using System.Collections;

public class MainMenuController : MonoBehaviour {

    public string targtScene;

    public OptionsMenuController optionesMenu;

    void Start()
    {
        optionesMenu.forceApply();
        if (PlayerController.instance!=null)
        {
            PlayerController.instance.gameObject.SetActive(false);
            Destroy(PlayerController.instance.gameObject);
            PlayerController.instance = null;
        }
    }

	public void startGame()
    {
        //Debug.Log("startgaME");
        if (LevelManager.ActiveLevel == null) Debug.Log("noLevelManaGER");
        if (targtScene == null) Debug.Log("no next scene");
        LevelManager.ActiveLevel.switchScene(targtScene);
    }

    public void quitGame()
    {
        Application.Quit();
    }



}
