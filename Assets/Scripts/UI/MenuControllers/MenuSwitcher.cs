﻿using UnityEngine;
using System.Collections;

public class MenuSwitcher : MonoBehaviour
{
    public MenuController targetMenu;

    private MenuController currentMenu;

    void Awake()
    {
        currentMenu = GetComponentInParent<MenuController>();
    }


    public void getClick()
    {
        currentMenu.hide();
        if (targetMenu != null)
        {
            targetMenu.display();

        }
    }
	
}
