﻿using UnityEngine;
using System.Collections;
using System;

public class PrinterSelectionMenuController : MenuController
{
    public PrintersListbox printersBox;
    
    public override void display()
    {
        target.SetActive(true);
        printersBox.initilize();
        printersBox.OnValueChange += printerConfirmed;
    }

    public override void hide()
    {
        printersBox.CleanListWraper();
        target.SetActive(false);
        printersBox.OnValueChange -= printerConfirmed;
    }

    public void printerConfirmed(int index)
    {
        if(printersBox.currentSelection!=null)
        {
            Vector3 printerPos = printersBox.currentSelection.output.position;
            PlayerController.instance.transportPlayer(new Vector3(printerPos.x,printerPos.y,PlayerController.instance.transform.position.z), false);
            LevelManager.ActiveLevel.activatePrinter(printersBox.currentSelection, false);
            //printersBox.initilize();
        }
    }
}
