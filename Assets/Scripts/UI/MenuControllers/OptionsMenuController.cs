﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Audio;

public class OptionsMenuController : MonoBehaviour {


    public AudioMixer mixer;
    /// <summary>
    /// the slider fo the master Volume setting
    /// </summary>
    public Slider masterAudioSlider;
    /// <summary>
    /// 
    /// </summary>
    public Slider SFCAudioSlider;
    /// <summary>
    /// the ratio between the controllers unit and the settings unit.
    /// </summary>
    /// <summary>
    /// the slider fo the master Volume setting
    /// </summary>
    public Slider musicAudioSlider;

    private bool initialUpdate = true;

    void Start()
    {
        
    }

    void Update()
    {
        if (initialUpdate)
        {
            masterAudioSlider.value = BotoSettingManager.getFloat(SettingsIndex.MasterVolume);
            SFCAudioSlider.value = BotoSettingManager.getFloat(SettingsIndex.SFXVolume);
            musicAudioSlider.value = BotoSettingManager.getFloat(SettingsIndex.MusicVolume);
            initialUpdate = false;
            applyAudioSettings();
        }
    }
    /// <summary>
    /// applies the settings to the <seealso cref="BotoSettingManager"/>
    /// </summary>
    public void applySettings()
    {
        
        BotoSettingManager.setFloat(SettingsIndex.MasterVolume, masterAudioSlider.value);
        BotoSettingManager.setFloat(SettingsIndex.SFXVolume, SFCAudioSlider.value);
        BotoSettingManager.setFloat(SettingsIndex.MusicVolume, musicAudioSlider.value);
        BotoSettingManager.save();
        applyAudioSettings();
    }

    private void applyAudioSettings()
    {
        setMasterVol();
        setSFXVol();
        setMusicVol();
    }
    /// <summary>
    /// applies changes to the audio mixer master group
    /// </summary>
    public void setMasterVol()
    {
        if (!initialUpdate)
        {
            mixer.SetFloat("MasterVolFloat", masterAudioSlider.value);
        }
    }

    /// <summary>
    /// applies changes to the audio mixer sfx group
    /// </summary>
    public void setSFXVol()
    {
        if (!initialUpdate)
        {
            mixer.SetFloat("SFXVolFloat", SFCAudioSlider.value);
        }
    }
    /// <summary>
    /// applies changes to the audio mixer sfx group
    /// </summary>
    public void setMusicVol()
    {
        if (!initialUpdate)
        {
            mixer.SetFloat("MusicVolFloat", musicAudioSlider.value);
        }
    }
    /// <summary>
    /// applies settings directlly without relating of contrllers.
    /// </summary>
    public  void forceApply()
    {
        mixer.SetFloat("MasterVolFloat", BotoSettingManager.getFloat(SettingsIndex.MasterVolume));
        mixer.SetFloat("SFXVolFloat", BotoSettingManager.getFloat(SettingsIndex.SFXVolume));
        mixer.SetFloat("MusicVolFloat", BotoSettingManager.getFloat(SettingsIndex.MusicVolume));
    }
}
