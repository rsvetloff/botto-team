﻿using UnityEngine;
using System.Collections;
using System;

public class BuildMenuManager : MenuController
{

    public InventorySelectorToggled weaponSelector;
    public InventorySelectorToggled passiveSelector;
    public InventorySelectorToggled thrustSelector;

    //private ItemIndex activeWeapon;
    //private ItemIndex activePassive;
    //private ItemIndex activeThrust;

    private RobotBuilder builder;

    void Awake()
    {
        builder = GameObject.FindGameObjectWithTag(TagsIndex.Player).GetComponentInParent<RobotBuilder>();
    }

    

    public override void display()
    {
        target.SetActive(true);
        weaponSelector.initilize();
        passiveSelector.initilize();
        thrustSelector.initilize();
        if (RobotBuilder.currentWeapon != ItemIndex.EmptyItem)
        {
            weaponSelector.setCurrentSelection(RobotBuilder.currentWeapon, true);
        }
        if (RobotBuilder.currentPassive != ItemIndex.EmptyItem)
        {
            passiveSelector.setCurrentSelection(RobotBuilder.currentPassive, true);
        }
       if (RobotBuilder.currentThruster != ItemIndex.EmptyItem)
       {

            thrustSelector.setCurrentSelection(RobotBuilder.currentThruster, true);
       }
        
    }

    public override void hide()
    {
        target.SetActive(false);
    }

    public void confirmPressed()
    {
        //activePassive = passiveSelector.CurrentSelection._itemCode;
        //activeThrust = thrustSelector.CurrentSelection._itemCode;
        //activeWeapon = weaponSelector.CurrentSelection._itemCode;
        builder.buildRobot(weaponSelector.CurrentSelection,passiveSelector.CurrentSelection,thrustSelector.CurrentSelection);

        hide();
    }

    


}
