﻿using UnityEngine;
using System.Collections;

public abstract class MenuController : MonoBehaviour
{
    public GameObject target;

    public abstract void display();

    public abstract void hide();
    
}
