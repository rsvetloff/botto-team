﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class CraftingMenuManager : MenuController
{
    #region inspector members
    /// <summary>
    /// the text component displaying the players current data balance.
    /// </summary>
    public Text currentDataText;
    /// <summary>
    /// the text component displaying the cost of current selected item.
    /// </summary>
    public Text currentCostText;
    /// <summary>
    /// the crafting ninventory selector.
    /// </summary>
    public InventoryController selector;
    /// <summary>
    /// the main crafting button text.
    /// </summary>
    public Text craftButtonText;
    /// <summary>
    /// a switcher to the build menu.
    /// </summary>
    public MenuSwitcher buildMenuSwitcher;

    private AudioSource craftAudio;

    public string dataPrefix;
    public string costPrefix;

    public string CraftText;
    public string UpgradeText;
    #endregion

    #region automated members

    public InventoryComponent playersInventory;
    #endregion
    /// <summary>
    /// the players current data balance.
    /// </summary>
    public float currentData;
    /// <summary>
    /// the cost of the current selected item.
    /// </summary>
    private float cost;

    private RobotBuilder builder;

    void Start()
    {
        playersInventory = PlayerController.instance.GetComponent<InventoryComponent>();
        builder = PlayerController.instance.GetComponent<RobotBuilder>();
        buildMenuSwitcher = GetComponent<MenuSwitcher>();
        craftAudio = GetComponent<AudioSource>();
    }


    public void craft()
    {
        if(selector.CurrentSelection!=null)
        {
            if(currentData>=cost)
            {
                craftAudio.Play();
                playersInventory.add(new InventoryItem(ItemIndex.Data_Normal, ItemCollationType.SingularAccumulate, -cost, true));
                playersInventory.add(new InventoryItem(selector.CurrentSelection));
                if(selector.CurrentSelection._replace!=ItemIndex.EmptyItem)
                {
                    playersInventory.remove(new InventoryItem(selector.CurrentSelection._replace, ItemCollationType.SingularReplace, 1, true));
                }
                updateCurrentData();
                builder.buildRobot(selector.CurrentSelection);
                selector.buildSelector();

            }
        }
    }

    public void updateCost(ItemPrerequisite selection)
    {
        
            if (selection != null)
            {
                InventoryItem data = selection.getPrerequisite(ItemIndex.Data_Normal);
                if (data != null)
                {
                    cost = data._quantity;
                }
                else
                {
                    cost = 0f;
                }
            }
            else
            {
                cost = 0f;
            }
        if (currentCostText != null)
        {
            currentCostText.text = costPrefix + cost;
        }
    }

    

    private void updateCurrentData()
    {
         
        currentData = playersInventory.lookAt(playersInventory.findItemIndex(ItemIndex.Data_Normal))._quantity;
        if (currentDataText != null)
        {
            currentDataText.text = dataPrefix + currentData;
        }
    }

    private void updateText(ItemPrerequisite item)
    {
        if (craftButtonText != null)
        {
            if (item == null || item._replace != ItemIndex.EmptyItem)
            {
                craftButtonText.text = CraftText;

            }
            else
            {
                craftButtonText.text = UpgradeText;
            }
        }

    }


   private void OnValueChange(int index)
    {
        
        updateCost(selector.CurrentSelection);
        updateText(selector.CurrentSelection);
    }

    public override void display()
    {
        target.SetActive(true);
        updateCurrentData();
        selector.initilize();
        updateText(null);
        updateCost(selector.CurrentSelection);
        
        selector.OnValueChange += OnValueChange;
    }

    public override void hide()
    {
        target.SetActive(false);
        selector.OnValueChange -= OnValueChange;
    }
}
