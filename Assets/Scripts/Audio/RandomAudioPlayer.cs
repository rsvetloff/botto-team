﻿using UnityEngine;
using System.Collections;

public class RandomAudioPlayer : MonoBehaviour {
    /// <summary>
    /// the audio source to play the clips
    /// </summary>
    [SerializeField]
    private AudioSource source;
    /// <summary>
    /// the posible audio clips the play randomlly
    /// </summary>
    [SerializeField]
    private AudioClip[] sounds;
    /// <summary>
    /// the time interval between checks in seconds
    /// </summary>
    [SerializeField]
    private float testDelay;
    /// <summary>
    /// the chance the a clip will play in check.
    /// </summary>
    [SerializeField]
    [Range(0f,1f)]
    private float playChance;
    
    public bool counting;

    private float counter = 0f;

    private void Update()
    {
        if(counting && !source.isPlaying)
        {
            counter += Time.deltaTime;
        }
        if(counter>=testDelay)
        {
            counter = 0f;
            playCheck();
        }
    }

    private void playCheck()
    {
        if(Random.Range(0f,1f)<=playChance)
        {
            source.clip = sounds[Random.Range(0, sounds.Length)];
            source.Play();
        }
    }
}
