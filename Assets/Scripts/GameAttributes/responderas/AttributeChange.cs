﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Contains data on as change to a <seealso cref="GameAttribute"/> contained in a <seealso cref="GameAttributeManager"/>
/// </summary>
public class AttributeChange
{
    private float _oldValue;
    /// <summary>
    /// The value of the attribute before the change.
    /// </summary>
    public float OldValue
    {
        get
        {
            return _oldValue;
        }
    }

    
    /// <summary>
    /// The value of the change.
    /// </summary>
    public float Modification
    {
        get
        {
            return _newValue-_oldValue;
        }
    }
    private float _newValue;
    /// <summary>
    /// THe new Value After the change.
    /// </summary>
    public float NewValue
    {
        get
        {
            return _newValue;
        }
    }

    private GameAttributeCodes _modifiedAttribute;
    /// <summary>
    /// the code of the attribute that was changed.
    /// </summary>
    public GameAttributeCodes ModifiedAttribute
    {
        get
        {
            return _modifiedAttribute;
        }
    }

    public AttributeChange(GameAttributeCodes code,float originalValue,float newValue)
    {
        _newValue = newValue;
        _modifiedAttribute = code;
        _oldValue = originalValue;
    }
}
