﻿using UnityEngine;
using System.Collections;

/// <summary>
/// the base class for responders to attribute changes
/// </summary>
public abstract  class AttributeChangeResponder : MonoBehaviour
{

	protected virtual void Start()
    {
        GameAttributeManager attachedManager = GetComponent<GameAttributeManager>(); //aquire the attribute manager and register for changes
        if(attachedManager !=null)
        {
            attachedManager.OnAttributeMofified += getAttributeChange;
        }
    }
    /// <summary>
    /// respond to changes in the attributes
    /// </summary>
    /// <param name="change"></param>
    protected abstract void getAttributeChange(AttributeChange change);
	
}
