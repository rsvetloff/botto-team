﻿using UnityEngine;
using System.Collections;
/// <summary>
/// Timmed game attribute.
/// a game attribute with specific time to live
/// </summary>
public class TimmedGameAttribute : GameAttribute 
{

	protected float effectTime=0f;

	public TimmedGameAttribute(GameAttributeCodes code) :base(code)
	{

	}


	public TimmedGameAttribute(GameAttributeCodes code, float ttl) : base(code)
	{
		effectTime = ttl;
	}


	public TimmedGameAttribute(GameAttributeCodes code,string stringValue,float floatValue) :base (code,stringValue,floatValue)
	{

	}

	/// <summary>
	/// Update this instance.
	/// </summary>
	void Update()
	{
		if (effectTime > 0) {
			effectTime -= Time.deltaTime;
		} 
		else 
		{
			collate = false;
		}
	}

	/// <summary>
	/// returns the state of the attribute;
	/// </summary>
	/// <returns><c>true</c>, if the attribute has not timmed out, <c>false</c> otherwise.</returns>
	public bool isAlive()
	{
		return effectTime>0;
	}
}
