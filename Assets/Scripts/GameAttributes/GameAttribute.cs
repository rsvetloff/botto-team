﻿using UnityEngine;
using System.Collections;
using System;

[System.Serializable]
/// <summary>
/// Attribute. represents an attibute of an object in play.
/// </summary>
public  class GameAttribute : IComparable
{

	//private static int _nextID = int.MinValue;

	public const string DELIMITER = "|";

	[SerializeField]
	/// <summary>
	/// The _code.
	/// the atribute code as is defined in AttributeIndex
	/// </summary>
	 protected GameAttributeCodes _code;

	/// <summary>
	/// The _string value.
	/// the value of the attribute.
	/// </summary>
	public string _stringValue;

	/// <summary>
	/// The _int value.
	/// the value of the attribute
	/// </summary>
	public float _floatValue;

	/// <summary>
	/// Uniqe ID for this instance of gameAttributes
	/// </summary>
	//public readonly int ID;

	/// <summary>
	/// The type of collation the attribute uses.
	/// </summary>
	public GameAttributeCollateType _collateType=GameAttributeCollateType.StringAttribute;

	/// <summary>
	/// designates if this attribut can be collated.
	/// </summary>
	public bool collate=true;

	/// <summary>
	/// Initializes a new instance of the <see cref="GameAttribute"/> class.
	/// </summary>
	/// <param name="code">Code of the attribute as defines in <see cref="AttributeIndex"/>.</param>
	/// <param name="value">the value of the attribute</param>
	public GameAttribute (GameAttributeCodes code)
	{
		_code = code;
		//ID = _nextID;
		//_nextID++;
		//_stringValue = value;
	}

	/// <summary>
	/// Initializes a new instance of the <see cref="GameAttribute"/> class.
	/// </summary>
	/// <param name="code">Code.</param>
	/// <param name="stringValue">String value.</param>
	/// <param name="floatValue">Float value.</param>
	public GameAttribute(GameAttributeCodes code,string stringValue,float floatValue)
	{
		_code = code;
		_stringValue = stringValue;
		_floatValue = floatValue;
		//ID = _nextID;
		//_nextID++;
	}

	/// <summary>
	/// Copy Constructor for  <see cref="GameAttribute"/> class.
	/// </summary>
	/// <param name="att">the attribute to copy.</param>
	public GameAttribute(GameAttribute att)
	{
		_code = att._code;
		_stringValue = att._stringValue;
		_floatValue = att._floatValue;
		//ID = _nextID;
		//_nextID++;
	}


	/// <summary>
	/// Gets the attribute's code.
	/// </summary>
	/// <returns>The attribute code.</returns>
	public GameAttributeCodes getCode()
	{
		return _code;
	}

	/// <summary>
	/// Sets the code of the attribue
	/// </summary>
	/// <param name="newCode">New code.</param>
	public void setCode(GameAttributeCodes newCode)
	{
		_code = newCode;
		_collateType = AttributesIndex.getAttributeCollateType (newCode);
	}

	/// <summary>
	/// Sets the code of the attribue
	/// </summary>
	/// <param name="newCode">New code.</param>
	public void setCode(int newCode)
	{
		setCode ((GameAttributeCodes)newCode);
	}

	/// <summary>
	/// Sets the string value of the attribute.
	/// </summary>
	/// <param name="newValue">New value for the attribute.</param>
	public void setValue(string newValue)
	{
		_stringValue = newValue;

	}

	/// <summary>
	/// Sets the float value of the attribute.
	/// </summary>
	/// <param name="newValue">New value for the attribute.</param>
	public void setValue(float newValue)
	{
		_floatValue = newValue;
		//_stringValue = newValue.ToString ();
	}

	/// <summary>
	/// Sets the int value of the attribute.
	/// </summary>
	/// <param name="newValue">New value for the attribute.</param>
	public void setValue(int newValue)
	{
		_floatValue = (float)newValue;
		_stringValue = newValue.ToString ();
	}

	/// <summary>
	/// sums the int value of the attribute.
	/// </summary>
	/// <param name="newValue"> value to sum to the attribute.</param>
	public void appendValue(int newValue)
	{
		_floatValue += (float)newValue;
		//_stringValue = (_floatValue).ToString ();
	}

	/// <summary>
	/// concate the string value of the attribute.
	/// </summary>
	/// <param name="newValue">New value to concate the attribute.</param>
	public void appendValue(string newValue)
	{
		_stringValue += DELIMITER +  newValue;
	}
	
	/// <summary>
	/// sums the float value of the attribute.
	/// </summary>
	/// <param name="newValue">New value to sum the attribute.</param>
	public void appendValue(float newValue)
	{
		_floatValue += newValue;
		//_stringValue = (_floatValue).ToString ();
	}
	


	/// <summary>
	/// Gets the value of the attribute.
	/// </summary>
	/// <returns>The attributes value.</returns>
	public string getAttributeString()
	{
		return _stringValue;
	}

	/// <summary>
	/// Gets the value of the attribute as integer.
	/// </summary>
	/// <returns>The attributes value as integer.</returns>
	public  int getAttributeInt()
	{
		return (int)_floatValue;
	}

	/// <summary>
	/// Gets the value of the attribute as float.
	/// </summary>
	/// <returns>The attributes value as float.</returns>
	public float getAttributeFloat()
	{
		return _floatValue;
	}

	public int CompareTo(System.Object obj)
	{
		return _code - ((GameAttribute)obj)._code;
	}

	public void resetValues()
	{
		_stringValue = "";
		_floatValue = 0f;
	}
}
