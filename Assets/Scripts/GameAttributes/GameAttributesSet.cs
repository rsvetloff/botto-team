﻿using UnityEngine;
using System.Collections;
using System;

/// <summary>
/// Attributes. a set of game attributes belonging to a game object.
/// </summary>
[System.Serializable]
public class GameAttributesSet : MonoBehaviour 
{
#if UNITY_EDITOR 
	[HideInInspector]
	public bool[] inspectorCollapsed;

	public GameAttribute[] getAttributes
	{
        get
        {
            return _attributes;
        }
		set
        {
            _attributes = value;
        }
	}
	
#endif
	[SerializeField]
	[HideInInspector]
	/// <summary>
	/// the array containing the game attributes
	/// </summary>
	protected GameAttribute[] _attributes=new GameAttribute[0];

	[SerializeField]
	[HideInInspector]
	public bool topCollator=false;
	

	public void Start()
	{
		Array.Sort (_attributes);
	}

	/// <summary>
	/// Resize the specified the attribute list keeping as much of the data as possible.
	///  extra cells in expension are filled with empty attributes.
	/// </summary>
	/// <param name="newSize">New size. the new size of the attribute list</param>
	public virtual void resize(int newSize)
	{
		int Size = newSize >= 0 ? newSize : 0;
		//Debug.Log (Size);
		if (Size != _attributes.Length) 
		{
			GameAttribute[] newAttributes = new GameAttribute[Size];
			for (int i=0;i<Size;i++)
			{
				if(i<_attributes.Length)
				{
					newAttributes[i] = _attributes[i];
				}
				else
				{
					newAttributes[i] = new GameAttribute(GameAttributeCodes.EMPTY_ATTRIBUTE); 
				}
			}
			_attributes=newAttributes;
		}
	}

	/// <summary>
	/// Gets the index of the attribute matching the given code i
	/// </summary>
	/// <returns>The attribute index.</returns>
	/// <param name="code">Code.</param>
	public int getAttributeIndex(GameAttributeCodes code)
	{
		int top = _attributes.Length - 1;
		int bottom = 0;
		int pointer = (top+bottom)/2;
		
		while (top>=bottom) 
		{
			if(_attributes[pointer].getCode()==code)
			{
				return pointer;
			}
			
			if(_attributes[pointer].getCode()<code)
			{
				bottom = pointer+1;
			}
			else
			{
				top = pointer-1;
			}
			pointer = (top-bottom)/2;
		}
		return -1;
	}


	protected virtual void collate(GameAttributesSet[] collateSets)
	{
		//Debug.Break ();
		Debug.Log ("collating");
		bool [] resets;
		if (topCollator) {
			resets = new bool[_attributes.Length];
		} 
		else 
		{
			resets = new bool[0];
		}
		foreach (GameAttributesSet set in collateSets) 
		{
			for (int i=0;i<_attributes.Length;i++)
			{
				GameAttribute att = set.getAttributeByCode(_attributes[i].getCode());
				if(att!=null)
				{
					if((!resets[i])&&topCollator)
					{
						resets[i]=true;
						_attributes[i].resetValues();
					}
					_attributes[i].appendValue(att.getAttributeFloat());
					_attributes[i].appendValue(att.getAttributeString());
				}
			}
		}
	}
	

	/// <summary>
	/// Gets an attribute with the given code.
	/// </summary>
	/// <returns>The attribute's to retuen code.</returns>
	/// <param name="code">Code.</param>
	protected GameAttribute getAttributeByCode(GameAttributeCodes code)
	{

		int top = _attributes.Length - 1;
		int bottom = 0;
		int pointer = (top+bottom)/2;

		while (top>=bottom) 
		{
			if(_attributes[pointer].getCode()==code)
			{
				return _attributes[pointer];
			}

			if(_attributes[pointer].getCode()<code)
			{
				bottom = pointer+1;
			}
			else
			{
				top = pointer-1;
			}
			pointer = (top+bottom)/2;
		}
		return null;
		/*
		for (int i=0; i<_attributes.Length; i++) 
		{
			if(_attributes[i].getCode()==code)
			{
				return _attributes[i];
			}
		}
		re-turn null;*/
	}

	/// <summary>
	/// gets the string value of the attribute with the given code from the set.
	/// </summary>
	/// <returns>The string value of the matching attrribute
	///  ifn there is no match in the set returns an empty string</returns>
	/// <param name="code">Code.</param>
	public string getAttributeString(GameAttributeCodes code)
	{

		GameAttribute att = getAttributeByCode ( code);
		if (att != null) 
		{
			return att.getAttributeString();
		}
		return "";
	}

	/// <summary>
	/// gets the int value of the attribute with the given code from the set.
	/// </summary>
	/// <returns>The int value of the matching attrribute
	///  ifn there is no match in the set returns an empty string</returns>
	/// <param name="code">Code.</param>
	public int getAttributeiInt(GameAttributeCodes code)
	{
		
		GameAttribute att = getAttributeByCode ( code);
		if (att != null) 
		{
			return att.getAttributeInt();
		}
		return 0;
	}


	/// <summary>
	/// gets the float value of the attribute with the given code from the set.
	/// </summary>
	/// <returns>The float value of the matching attrribute
	///  ifn there is no match in the set returns an empty string</returns>
	/// <param name="code">Code.</param>
	public float getAttributeiFloat(GameAttributeCodes code)
	{
		
		GameAttribute att = getAttributeByCode ( code);
		if (att != null) 
		{
			return att.getAttributeFloat();
		}
		return 0f;
	}

	/// <summary>
	/// Gets the attribute count.
	/// </summary>
	/// <returns>The attribute count in the set.</returns>
	public int getAttributeCount ()
	{
		return _attributes.Length;
	}
}
