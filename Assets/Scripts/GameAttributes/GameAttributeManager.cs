﻿using UnityEngine;
using System.Collections;

public class GameAttributeManager : GameAttributesSet 
{
#if UNITY_EDITOR 
	public float[] getModifiers()
	{
		return _modifiers;
	}
#endif

    public delegate void AttributeModified(AttributeChange change);
    public event AttributeModified OnAttributeMofified;

	[SerializeField]
	[HideInInspector]
	/// <summary>
	/// The current modifiers applied to managed attributes.
	/// </summary>
	protected float[] _modifiers;



	new void Start()
	{
		base.Start ();
		if (_modifiers == null) 
		{
			_modifiers = new float[_attributes.Length];
		}
	}

	/// <summary>
	/// Resize the specified the attribute list keeping as much of the data as possible.
	///  extra cells in expension are filled with empty attributes.
	/// </summary>
	/// <param name="newSize">New size. the new size of the attribute list</param>
	public override void resize (int newSize)
	{
		int Size = newSize >= 0 ? newSize : 0;
		//Debug.Log (Size);
		if (Size != _attributes.Length) 
		{
			GameAttribute[] newAttributes = new GameAttribute[Size];
			float[] newMods = new float[Size];

			for (int i=0;i<Size;i++)
			{
				if(i<_attributes.Length)
				{
					newAttributes[i] = _attributes[i];
					newMods[i] = _modifiers[i];
				}
				else
				{
					newAttributes[i] = new GameAttribute(GameAttributeCodes.EMPTY_ATTRIBUTE); 
				}
			}
			_attributes=newAttributes;
			_modifiers = newMods;
		}
	}


	/// <summary>
	/// Collate the specified collateSets.
	/// </summary>
	public  void collate()
	{
		base.collate (getCollationSet ());
	}

	/// <summary>
	/// Retrives the AttributeSets Collated by the manger.
	/// </summary>
	/// <returns>The AttributeSets collated by the manager.</returns>
	public GameAttributesSet[] getCollationSet()
	{
		return transform.parent.GetComponentsInChildren<GameAttributesSet> ();
	}

    public void modifiyAttributeValue(GameAttributeCodes attributeCode, float modifier)
    {
        GameAttribute attribute = getAttributeByCode(attributeCode);
        if(attribute!=null)
        {
            setAttributeValue(attribute, attribute._floatValue - modifier);
            
        }
    }

    public void setAttributeValue(GameAttributeCodes attributeCode,float newValue)
    {
        GameAttribute attribute = getAttributeByCode(attributeCode);
        if (attribute != null)
        {
            setAttributeValue(attribute, newValue);

        }
    }

    private void setAttributeValue(GameAttribute attribute,float newValue)
    {
        if (attribute != null && !GameManager.instance._lowPause)
        {
            attribute._floatValue = newValue;
            if (OnAttributeMofified != null)
            {
                OnAttributeMofified.Invoke(new AttributeChange(attribute.getCode(), attribute._floatValue, newValue));
            }

        }
    }
}
