﻿using UnityEngine;
using System.Collections;

public static class AttributesIndex 
{
	public  const int CURRENT_HP=1;
	public  const int MAX_HP=2;
	public const int MIN_HP=3;

	public const int CURRENT_SPEED=10;
	public const int MAX_SPEED=11;



	public static string getAttributeName(int code)
	{
		switch (code) 
		{
		case 1: return "Current HP";
		case 2: return "Max HP";
		case 3: return "Min HP";
		case 10: return "Current Speed";
		case 11: return "Max Speed";
		case 0: return "Empty Attribute";
		default: return "Null Attribute";
		}
	}

	public static GameAttributeCollateType getAttributeCollateType(GameAttributeCodes code)
	{
		switch ((int)code) 
		{
		case 1:
		case 2:
		case 3:
			return GameAttributeCollateType.intAttribute;
			//break;
		case 10:
		case 11:
			return GameAttributeCollateType.floatAttribute;
			//break;
		default:
			return GameAttributeCollateType.StringAttribute;
		}
	}
}
