﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

[System.Serializable]
public class InventoryItemBase
{
    #region inspector members

    public enum InventoryItemTypes
    {
        General=0,
        Weapon = 1,
        Passive = 2,
        Thruster =3
    }

    /// <summary>
    /// The item code.
    /// </summary>
    public ItemIndex _itemCode;
    /// <summary>
    /// the type of item.
    /// </summary>
    public InventoryItemTypes _itemType;

    public string _displayName;

    /// <summary>
    /// the image used when displaying this item in the UI.
    /// </summary>
    public Sprite _UIImage;
    /// <summary>
    /// the prefab used when this item is instantiated to the scene
    /// </summary>
    public GameObject _prefab;
    /// <summary>
    /// the ui sidcription
    /// </summary>
    public string _description="";

    public ItemIndex _replace;
    #endregion
    public InventoryItemBase()
    {

    }

    public InventoryItemBase(ItemIndex code,
                            string name,
                            string Description,
                            Sprite image,
                            GameObject prefab)
    {

        _itemCode = code;
        _displayName = name;
        _description = Description;
        _UIImage = image;
        _prefab = prefab;
    }

    public override string ToString()
    {
        if(_displayName==null || _displayName=="")
        {
            return _itemCode.ToString();
        }
        return _displayName;
    }

}
