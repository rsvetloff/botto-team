﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Comparers  
    {


    public class ItemPrerequisitesNameComparer : IComparer<ItemPrerequisite>
    {

        public int Compare(ItemPrerequisite x, ItemPrerequisite y)
        {
            return string.Compare(x._displayName, y._displayName);
        }
    }

    public class ItemPrerequisitesNumberComparer : IComparer<ItemPrerequisite>
    {

        public int Compare(ItemPrerequisite x, ItemPrerequisite y)
        {

            if (x._itemCode > y._itemCode) return 1;
            if (x._itemCode < y._itemCode) return -1;
            return 0;
        }
    }
}
