﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

[System.Serializable]
/// <summary>
/// tnis class represnts Prerequisite for crafting of inventory items. 
/// </summary>
public class ItemPrerequisite  :InventoryItem
{


	#if UNITY_EDITOR 
	[HideInInspector]
	public List<bool> inspectorCollapsed;

	
#endif
    /*
    /// <summary>
    /// The item code.
    /// </summary>
    public ItemIndex _itemCode;

    /// <summary>
    /// The type of the collation.
    /// </summary>
    public ItemCollationType _collationType;
    /// <summary>
    /// The quantity of the item in the inventory.
    /// </summary>
    public float _quantity;
    /// <summary>
    /// a flag indicating if this InventoryItem can be taken by other inventory
    /// </summary>
    public bool _collectable;

    public string _displayName;

    /// <summary>
    /// the image used when displaying this item in the UI.
    /// </summary>
    public Sprite _UIImage;
    /// <summary>
    /// the prefab used when this item is instantiated to the scene
    /// </summary>
    public GameObject _prefab;
    /// <summary>
    /// The prerequisites for this item.
    /// </summary>
    */
    public Inventory prerequisites;
	/// <summary>
	/// The iteration step. this is the next prerequisite that will be iterated
	/// </summary>
	private int iterationStep;

    public ItemPrerequisite ()
    {
       prerequisites = new Inventory();
    }

	/// <summary>
	/// Initializes a new instance of the <see cref="ItemPrerequisite"/> class.
	/// </summary>
	/// <param name="code">the ItemCode for which the prerequisites apply</param>
	public ItemPrerequisite (ItemIndex code,  
                             bool collectable
                                ) :base(code,ItemCollationType.SingularReplace,1f,true)
	{
        /*
        _itemCode = code;
        _displayName = displayName;
        _collectable = collectable;
        _UIImage = image;
        _prefab = prefab;
        */
		prerequisites = new Inventory ();
        //_itemCode = code;
    }


    public ItemPrerequisite (ItemPrerequisite other) :base(other)
    {
        /*
        _itemCode = other._itemCode;
        _collationType = other._collationType;
        _quantity = other._quantity;
        _collectable = other._collectable;
        _displayName = other._displayName;
        _UIImage = other._UIImage;
        _prefab = other._prefab;
        */
        prerequisites = new Inventory();
        _collationType = ItemCollationType.Multiple;
        _collectable = false;
       
    }



	
	/// <summary>
	/// Adds a prerequisite.\n
	/// Iteration will be reset after this call.
	/// </summary>
	/// <param name="item">The item to add s a prerequisite. Note the quantity is also factored when evaluating prerequisites</param>
	public void addPrerequisite(InventoryItem item)
	{
		item._collationType = ItemCollationType.SingularReplace;
		prerequisites.add (item);
		beginIterartion ();
	}

    public void addPrerequisite(ItemIndex code,float quantity,bool collectable)
    {
        prerequisites.add( new InventoryItem(code, ItemCollationType.SingularReplace, quantity, collectable));

    }

    /// <summary>
    /// Removes the prerequisite./n
    /// Iteration will be reset after this call.
    /// </summary>
    /// <param name="item">Item.</param>
    public void removePrerequisite(InventoryItem item)
	{
		prerequisites.remove (item);
		beginIterartion ();
	}

	/// <summary>
	/// Begins the iterartion. after this call the next prerequisite is the first one.
	/// </summary>
	public void beginIterartion()
	{
		iterationStep = 0;
	}
	/// <summary>
	/// checks if the iteration has a next step.
	/// </summary>
	/// <returns><c>true</c>, if next was hased, <c>false</c> otherwise.</returns>
	public bool hasNext()
	{
		return iterationStep < prerequisites.Count;
	}

	/// <summary>
	/// Gets the next prerequisite in the iteration.
	/// </summary>
	/// <returns>The next prerequisite in the iteration. /n 
	/// if no more prerequisites exist returns null.</returns>
	public InventoryItem getNext()
	{
		if (hasNext ()) 
		{
            
            InventoryItem result = new InventoryItem(prerequisites.lookAt(iterationStep));
            iterationStep++;
            return result;
            
		}
		return null;
	}


    public InventoryItem getPrerequisite(ItemIndex code)
    {
        return prerequisites.getItemByCode(code);
    }
}
