﻿
using UnityEngine;
using System.Collections;

public enum ItemIndex  
{
	EmptyItem=0,
	//data
	Data_Normal=0001,

	//weapon plans
	Plans_Shotgun_1=1001,
	Plans_Shotgun_2=1002,
	Plans_Shotgun_3=1003,
	Plans_Grende_1=1101,
	Plans_Grende_2=1102,
	Plans_Grende_3=1103,
	Plans_Laser_1=1201,
	Plans_Laser_2=1202,
	Plans_Laser_3=1203,
    Plans_Sniper_1=1301,

    //weapons
    Shotgun_1=1011,
    Shotgun_2 = 1012,
    Sniper_1 = 1031,

    //thrust plans
    Plans_Hover_1 =2001,
	Plans_Hover_2=2003,
	Plans_Hover_3=2003,
	Plans_Crawler_1=2101,
	Plans_Crawler_2=2102,
	Plans_Crawler_3=2103,
	Plans_Legs_1=2201,
	Plans_Legs_2=2202,
	Plans_Legs_3=2203,

    //thursts
    Hover_1 = 2011,
    Hover_2 = 2012,
    Crawler_1 = 2111,
    Crawler_2 = 2112
}
