﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

/// <summary>
/// this class represents a editable lis of prerequisites for inventory items
/// </summary>
public class itemPrerequisiteManager : MonoBehaviour
{

#if UNITY_EDITOR

    public List<bool> foldout;

    public List<ItemPrerequisite> getprerequisites()
    {
        return prerequisites;
    }

    public void setPrerequisites(List<ItemPrerequisite> newPrerequisites)
    {
        prerequisites = newPrerequisites;
    }


#endif


    /// <summary>
    /// The prerequisite list
    /// </summary>
    public List<ItemPrerequisite> prerequisites;

    /// <summary>
    /// Sortes the prerequisites by item name;
    /// </summary>
    public void sortPrerequsitesByItemName()
    {
        sortPrerequisites(new Comparers.ItemPrerequisitesNameComparer());
    }

    /// <summary>
    /// Sortes the prerequisites by item name;
    /// </summary>
    public void sortPrerequisitesByDisplayNames()
    {
        sortPrerequisites(new Comparers.ItemPrerequisitesNumberComparer());
    }


    private void sortPrerequisites(IComparer<ItemPrerequisite> comparer)
    {
        prerequisites.Sort(comparer);
    }


    public ItemPrerequisite addItem(ItemIndex code,bool collectable)
    {
        int index = findItemIndex(code);
        ItemPrerequisite result = null;
        if (index==-1)
        {
            result = new ItemPrerequisite(code, collectable);
            prerequisites.Add(result);
            return result;
            //sortPrerequisitesByDisplayNames();
        }
        return prerequisites[index];
    }

    /// <summary>
    /// adds a prerequisite to the the manager
    /// </summary>
    /// <param name="code">the code of the item the prerequisite applies to</param>
    /// <param name="itemName">the name of the item</param>
    /// <param name="prerequisiteCode">the code of the prerequisite item</param>
    /// <param name="prerequisiteName"></param>
    /// <param name="prerequisiteQuantity"></param>
    /// <param name="collecable"></param>
    /// <param name="image"></param>
    /// <param name="prefab"></param>
    public void addPrerequiite(ItemIndex code,
                                ItemIndex prerequisiteCode,
                                float prerequisiteQuantity, 
                                bool collecable
)
    {
        ItemPrerequisite preq = addItem(code, collecable);
        preq.addPrerequisite(prerequisiteCode,prerequisiteQuantity, collecable);
    }


    /// <summary>
    /// removew an item from the prerequisite manager
    /// </summary>
    /// <param name="code">the item code to remove.</param>
    public void removeItem(ItemIndex code)
    {
        int index = findItemIndex(code);
        if(index!=-1)
        {
            prerequisites.RemoveAt(index);
        }
    }
    /// <summary>
    /// the number of items whos prerequisites are stor4ed in the manager.
    /// </summary>
    public int Count
    {
        get { return prerequisites.Count; }
    }


    private int findItemIndex(ItemIndex code)
    {
        for (int i = 0; i < prerequisites.Count; i++)
        {
            if (prerequisites[i]._itemCode == code)
            {
                return i;
            }
        }
        return -1;
    }
    /// <summary>
    /// gets the <seealso cref="ItemPrerequisite"/> from the manager suitable for the guiven <seealso cref="InventoryItem"/>
    /// </summary>
    /// <param name="item">the item to fech the prerequisites for</param>
    /// <returns>the <seealso cref="ItemPrerequisite"/> that coresponds to the item</returns>
    public ItemPrerequisite getPrereauisite(InventoryItem item)
    {
        int index = findItemIndex(item._itemCode);
        if(index==-1)
        {
            return null;
        }
        return prerequisites[index];
    }

}
