﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

[System.Serializable]
public class InventoryItem
{
    #region inspector members



    /// <summary>
    /// The item code.
    /// </summary>
    public ItemIndex _itemCode;

    /// <summary>
    /// The type of the collation.
    /// </summary>
    public ItemCollationType _collationType;
	/// <summary>
	/// The quantity of the item in the inventory.
	/// </summary>
	public float _quantity;
	/// <summary>
	/// a flag indicating if this InventoryItem can be taken by other inventory
	/// </summary>
	public bool _collectable;

    public string _displayName
    {
        get
        {
            return datatbase.getItemData(_itemCode)._displayName;
        }
    }

    /// <summary>
    /// the image used when displaying this item in the UI.
    /// </summary>
    public Sprite _UIImage
    {
        get
        {
            return datatbase.getItemData(_itemCode)._UIImage;
        }
    }
    /// <summary>
    /// the prefab used when this item is instantiated to the scene
    /// </summary>
    public GameObject _prefab
    {
        get
        {
            return datatbase.getItemData(_itemCode)._prefab;
        }
    }
    /// <summary>
    /// the ui sidcription
    /// </summary>
    public string _description
    {
        get
        {
            return datatbase.getItemData(_itemCode)._description;
        }
    }
    /// <summary>
    /// the type of the item
    /// </summary>
    public InventoryItemBase.InventoryItemTypes _type
    {
        get
        {
            return datatbase.getItemData(_itemCode)._itemType;
        }
    }
    /// <summary>
    /// the item superceded by this item.
    /// </summary>
    public ItemIndex _replace
    {
        get
        {
            return datatbase.getItemData(_itemCode)._replace;
        }
    }

    public static ItemsDataBase datatbase;

//	/// <summary>
//	/// a flag indicating if the InventoryItem is enabled and can be used.
//	/// </summary>
//	public bool _enabled;

	#endregion

	#region Constructors
	public InventoryItem (ItemIndex code,ItemCollationType collation,float quantity,bool collectable)
	{
		_itemCode = code;
        _quantity = quantity;
        _collationType = collation;
        _collectable = collectable;
        
	}
    public InventoryItem()
    {
        
    }

    public InventoryItem(InventoryItem other)
    {
        _itemCode = other._itemCode;
        _quantity = other._quantity;
        _collationType = other._collationType;
        _collectable = other._collectable;
    }
    #endregion



    public override string ToString ()
	{
		return _itemCode.ToString () + "-" + _quantity;
	}

    public override bool Equals(object obj)
    {
        if(! (obj is InventoryItem))
        {
            return false;
        }
        InventoryItem otherItem = (InventoryItem)obj;
        if(_itemCode==otherItem._itemCode)
        {
            return true;
        }
        else
        {
            return false;
        }
    }


    public override int GetHashCode()
    {
        return _itemCode.GetHashCode();
    }

    public string getDisplayName()
    {
        if (_displayName=="")
        {
            return _itemCode.ToString();
        }
        else
        {
            return _displayName;
        }
    }
}
