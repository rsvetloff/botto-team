﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class ItemsDataBase : MonoBehaviour
{
    public static ItemsDataBase instance;
#if UNITY_EDITOR
    [SerializeField]
    public List<bool> inspectorCollapsed;





#endif
    
  
    public List<InventoryItemBase> itemsData;	


    void Awake()
    {
        if(instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);

        }
        else
        {
            Destroy(gameObject);
        }
    }

    public bool addItem(InventoryItemBase item)
    {
        int index = findItemIndex(item._itemCode);
 
        if(index>-1)
        {
            itemsData.RemoveAt(index);
            
        }
        itemsData.Add(item);
        return index == -1;
    }


    public bool removeItem(InventoryItemBase item)
    {
        int index = findItemIndex(item._itemCode);
        if(index>-1)
        {
            itemsData.RemoveAt(index);
        }
        return index == -1;
    }

    public InventoryItemBase getItemData(ItemIndex code)
    {
        int index = findItemIndex(code);
        if (index > -1)
        {
            return itemsData[index];
        }
        return null;
    }

    private int findItemIndex(ItemIndex code)
    {
        int result = -1;
        for (int i=0;i<itemsData.Count;i++)
        {
            if(itemsData[i]._itemCode==code)
            {
                result = i;
                break;
            }
        }
        return result;
    }

    public void saveToFile()
    {
        if (itemsData != null)
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.dataPath + "/gamedata/Items.dat", FileMode.OpenOrCreate);
            DataStore store = new DataStore();
            store.itemsData = itemsData;
            bf.Serialize(file, store);
            file.Close();
        }
    }

    public void loadFromFile()
    {
        if(File.Exists(Application.dataPath + "/gamedata/Items.dat"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.dataPath + "/gamedata/Items.dat", FileMode.Open);
            DataStore store = (DataStore)bf.Deserialize(file) ;
            file.Close();
            itemsData = store.itemsData;
        }
    }


}
[Serializable]
class DataStore
{
    public List<InventoryItemBase> itemsData;
}