﻿using UnityEngine;
using System.Collections;

public enum ItemCollationType  
{
	/// <summary>
	/// This mode will keep all instances of an item added to an inventory.
	/// </summary>
	Multiple=0,
	/// <summary>
	/// This mode will always retain the first instance added to an inventory.
	/// </summary>
	SingularRetaine=1,
	/// <summary>
	/// Thgis mode will always retain the last instance added to an inventory.
	/// </summary>
	SingularReplace=2,
	/// <summary>
	/// This mode will sum the amount of each added item to an inventory.
	/// </summary>
	SingularAccumulate=3,



}
