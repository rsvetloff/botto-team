﻿using UnityEngine;
using System.Collections;
/// <summary>
/// Inventory collector.
/// theis class checks for triggers activation and attemps to collect inventories when possible.
/// </summary>
public class InventoryCollector : MonoBehaviour {

	#region Priate Members
	[SerializeField]
	private InventoryComponent _inventory;
    #endregion
    private AudioSource _aSource;
    public AudioClip collectionAudio;

    void Awake()
    {
        _aSource = GetComponent<AudioSource>();
    }


	void OnTriggerEnter(Collider other)
	{
		collect( other.GetComponentInParent<InventoryComponent> ());
		
	}

    void OnCollisionEnter(Collision collision)
    {
        collect(collision.gameObject.GetComponentInParent<InventoryComponent>());
    }

    private void collect(InventoryComponent inventory)
    {
        if (inventory != null && inventory.Collectable)
        {
            _aSource.clip = collectionAudio;
            _aSource.loop = false;
            _aSource.Play();
            _inventory.collect(inventory);
        }
    }
}
