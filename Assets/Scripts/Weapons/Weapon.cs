﻿using UnityEngine;
using System.Collections;
using System;

/// <summary>
/// the base class for weapons in the game.
/// </summary>
public abstract class Weapon : MonoBehaviour
{

    #region base values
    /// <summary>
    /// the max ammo that can be carried by the weapon
    /// </summary>
    public float AmmoCapacity;
    /// <summary>
    /// the recoil force applied when the weapon is fired.
    /// </summary>
    public float recoilForce;
    /// <summary>
    /// the max range of the weapon
    /// </summary>
    public float range;
    /// <summary>
    /// the HP damage inflicted by the weapon.
    /// </summary>
    public float damage;
    /// <summary>
    /// the time is seconds the weapon reload action takes.
    /// </summary>
    public float reloadTime;
    /// <summary>
    /// the amount of ammo that can be fired before relo0ad is needed.
    /// </summary>
    public float ammoClipCapacity;
    /// <summary>
    /// the rate of fire in shots per second.
    /// </summary>
    [Tooltip("Fire rate in shots per second")]
    public float fireRate;
    /// <summary>
    /// indicates the amount of pojectiles that are released when fired
    /// </summary>
    public int projectilesPerFire;
    /// <summary>
    /// the transforms that will point twards the target;
    /// </summary>
    public Transform[] poniters;
    /// <summary>
    /// the <seealso cref="HitEffect.HitGroups"/> affected by the weapon
    /// </summary>
    public HitEffect.HitGroups group;
    #endregion

    #region tracking values
    public float CurrentAmmo;
    public float CurrentClipAmmo;
    #endregion

    #region settings
    public AimController aim;
    public bool continuousFire;
    public ParticleSystem shotVisual;
    protected Rigidbody rbody;
    protected HitEffect[] shotEffects;
    #endregion

    protected float reloadTimer;
    protected bool reloading;
    protected float fireTimer;

    protected bool fired = false;

    private Vector3 lastShotDirection;
    public Vector3 LastShotDirection
    {
        get
        {
            return lastShotDirection;
        }
    }

    public AudioSource ShotSound;

    protected int shotID = 0;

    public int ShotID
    {
        get
        {
            return shotID;
        }
    }



    #region base implementation
    /// <summary>
    /// fires the weapon
    /// </summary>
    protected virtual void fire()
    {
        if (canFire()&& !GameManager.instance._lowPause)
        {
            fireTimer = 1 / fireRate;
            fired = true;
            shotVisual.Emit(projectilesPerFire);
            lastShotDirection = transform.forward;
            shotID++;
            rbody.AddForce(-transform.forward * recoilForce);
            ShotSound.Play();
        }
    }

    #endregion
    protected virtual void Awake()
    {
        rbody = GetComponentInParent<Rigidbody>();
        if (shotVisual == null)
        {
            shotVisual = GetComponent<ParticleSystem>();
        }
        initilize();
        ShotSound = GetComponent<AudioSource>();
        AttributeHitEffect damageEffect = gameObject.AddComponent<AttributeHitEffect>();
        damageEffect.affectedAttribute = GameAttributeCodes.CURRENT_HP;
        damageEffect.group = group;
        damageEffect.hitValue = damage;
        shotEffects = GetComponents<HitEffect>();
    }
    
    public void initilize()
    {
        rbody = GetComponentInParent<Rigidbody>();
        if (aim == null)
        {
            aim = GetComponentInParent<AimController>();
        }
    }

    protected virtual void reload()
    {
        if (CurrentAmmo>0)
        {
            if(CurrentAmmo>ammoClipCapacity - CurrentClipAmmo)
            {
                CurrentAmmo -= ammoClipCapacity - CurrentClipAmmo;
                CurrentClipAmmo = ammoClipCapacity;

            }
            else
            {
                CurrentClipAmmo += CurrentAmmo;
                CurrentAmmo = 0f;
            }
        }
    }

    protected virtual bool canFire()
    {
        if (continuousFire || !fired)
        {
            if (CurrentClipAmmo > 0 && fireTimer <= 0)
            {
                return true;
            }
        }
        return false;

    }

    public virtual void getCommands(CommandMask commands)
    {
        if(commands.containsCommand(UserCommands.FIRE))
        {
            if(continuousFire||(!fired))
            {
                fire();
                fired = true;
            }
        }

        else
        {
            fired = false;
        }
        if(commands.containsCommand(UserCommands.RELOAD))
        {
            reload();
        }
    }

    protected virtual void recoverShot()
    {
        if (fireTimer > 0)
        {
            fireTimer -= Time.deltaTime;
        }
    }

    protected virtual void Update()
    {
        recoverShot();
    }
}
