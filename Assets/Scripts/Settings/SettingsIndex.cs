﻿using UnityEngine;
using System.Collections;

public enum SettingsIndex
{
    NOTHING=0,
    MasterVolume = 1,
    SFXVolume= 2,
    MusicVolume=3
}
