﻿using UnityEngine;
using System.Collections;
/// <summary>
/// manages the settings of the game.
/// </summary>
public static class BotoSettingManager
{

    public delegate void settingChanged(SettingsIndex setting);
    /// <summary>
    /// this event triggers when a setting is changed.
    /// </summary>
    public static event settingChanged OnSettingChanged;
    /// <summary>
    /// gets the float calue of a setting.
    /// </summary>
    /// <param name="setting">the setting to retrive the value for.</param>
    /// <returns></returns>
    public static float getFloat(SettingsIndex setting)
    {
        return PlayerPrefs.GetFloat(setting.ToString());
    }
    /// <summary>
    /// updates the float value of a setting.
    /// </summary>
    /// <param name="setting">the setting to update</param>
    /// <param name="newValue">the new value to apply</param>
    public static void setFloat(SettingsIndex setting,float newValue)
    {
        if(setting != SettingsIndex.NOTHING)
        {
            PlayerPrefs.SetFloat(setting.ToString(), newValue);
            if (OnSettingChanged!=null)
            {
                OnSettingChanged.Invoke(setting);
            }
        }
    }

    /// <summary>
    /// Saves modifications to disk.
    /// \n note thatthis is not required for settings to be applied
    /// </summary>
    public static void save()
    {
        PlayerPrefs.Save();
    }
}
