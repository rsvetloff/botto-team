﻿using UnityEngine;
using System.Collections;
using System;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public  bool _lowPause;

    public RectTransform tutorialScreen;

    void Awake()
    {
        if(instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
            InventoryItem.datatbase = GameObject.FindGameObjectWithTag(TagsIndex.GameManager).GetComponent<ItemsDataBase>();
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            instance.tutorialScreen = tutorialScreen;
            instance._lowPause = _lowPause;
            Destroy(gameObject);
        }
        
    }

    

}
