﻿using UnityEngine;
using System.Collections;
/// <summary>
/// this class manages aspects tat are level unique 
/// </summary>
public class LevelManager : MonoBehaviour
{

    /// <summary>
    /// the menu controller of the level.
    /// </summary>
    public PrinterMenuManager printerMenu;
    public static LevelManager ActiveLevel;
    /// <summary>
    /// the printers that are currently active on thne level.
    /// </summary>
    public PrinterTriggerScript[] printers;

    /// <summary>
    /// the current activfe printer.
    /// </summary>
    [SerializeField]
    [Tooltip("the ative printer index in the level")]
    private int activePrinterIndex;
    public int ActivePrinterIndex
    {
        get
        {
            return activePrinterIndex;
        }
    }
    /// <summary>
    /// indicates if the entrence transition should be made when the level is loaded.
    /// </summary>
    public bool makeEnterTransition;

    /// <summary>
    /// the status of the printers in the level matched by index to to the printers Array.
    /// </summary>
    [HideInInspector]
    public bool[] printerStatuses;
    /// <summary>
    /// the fade conponent used to faDE THE SCREEN IN AND OUT.
    /// </summary>
    public imageFader fader;
    /// <summary>
    /// the name of the next scene to transition to.
    /// </summary>
    private string _nextScene="";

    private AsyncOperation levelLoader;

    public PrinterTriggerScript ActivePrinter
    {
        get
        {
            return printers[activePrinterIndex];
        }
        set
        {
            for (int i = 0; i < printers.Length; i++)
            {
                if (printers[i] == value)
                {
                    activePrinterIndex = i;
                    printerStatuses[i] = true;
                    break;
                }
            }
        }
    }

    void Awake()
    {
        //Debug.Log(ActiveLevel == this);
        ActiveLevel = this;
        
        
    }


    // Use this for initialization
    void Start()
    {
        //GameManager.instance._lowPause = false;
        if (printers.Length > 0)
        {
            printerStatuses = new bool[printers.Length];
            if (activePrinterIndex < 0 || activePrinterIndex > printerStatuses.Length - 1)
            {
                activePrinterIndex = 0;
            }
            printerStatuses[activePrinterIndex] = true;
        }
        if(makeEnterTransition)
        {
            fader.fadeout(true);
        }
        this.enabled = false;
    }

    public void activatePrinter(PrinterTriggerScript printer,bool showUI)
    {
        int printerIndex = indexOf(printer);
        if(printerIndex>-1)
        {
            activePrinterIndex = printerIndex;
            printerStatuses[printerIndex] = true;
            if (showUI)
            {
                printerMenu.display();
            }
        }
    }

    void Update()
    {
        //Debug.Log("Update");
        if(_nextScene!="")
        {
            //if(levelLoader==null)
            //{
            //    StartCoroutine(switchScene());
            //}
            if(!fader.IsFading)
            {
                //Debug.Log("ende fade");
                //if(levelLoader.progress>=0.9f)
                //{

                //    levelLoader.allowSceneActivation = true;
                //    //ActiveLevel.fader.fadeout(true);
                //}
                if (PlayerController.instance != null)
                {
                    PlayerController.instance.transform.SetParent(null);
                }
                Application.LoadLevel(_nextScene);
            }
            
        }
    }

    /// <summary>
    /// retrives the index number of a given printer
    /// </summary>
    /// <param name="printer">the printer to retrive the inde for.</param>
    /// <returns></returns>
    private int indexOf(PrinterTriggerScript printer)
    {
        for (int i=0;i<printers.Length;i++)
        {
            if(printer==printers[i])
            {
                return i;
            }
        }
        return -1;
    }

    /// <summary>
    /// switches the game to another scene.
    /// </summary>
    /// <param name="nextScene">the name of the scene totransition to.</param>
    public void switchScene(string nextScene)
    {
        //Debug.Log("switch scene");
        fader.fadein(false);
        this.enabled = true;
        _nextScene = nextScene;
        
        //StartCoroutine(switchScene());
    }

    private IEnumerator switchScene()
    {
        levelLoader = Application.LoadLevelAsync(_nextScene);
        levelLoader.allowSceneActivation = false;
        yield return levelLoader;
        //Debug.Log("loaded");
    }


}
