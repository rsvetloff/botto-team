﻿using UnityEngine;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class fileSaver <T> 
{
    private string _filename;



    public fileSaver(string fileName)
    {
        _filename = fileName;
    }


    public void saveToFile(T data)
    {
        BinaryFormatter br = new BinaryFormatter();
        FileStream file = File.Open(Application.dataPath + "/gamedata/" + _filename + ".dat",FileMode.OpenOrCreate);
        br.Serialize(file, data);
        file.Close();
    }
	
    public T loadFromFile()
    {
        if (File.Exists(Application.dataPath + "/gamedata/" + _filename + ".dat"))
        {
            BinaryFormatter br = new BinaryFormatter();
            FileStream file = File.Open(Application.dataPath + "/gamedata/" + _filename + ".dat", FileMode.Open);
            T result = (T)br.Deserialize(file);
            file.Close();
            return result;
        }
        return default(T);
    }
}
