﻿using UnityEngine;
using System.Collections;
using System;

public class EnableResponder : RiddleResponder
{
    public MonoBehaviour[] effectList;
    public GameObject[] effectComponent;

    public override void respond(bool riddleSolved)
    {
        for (int i = 0;i< effectList.Length; i++)
        {
            effectList[i].enabled = riddleSolved;
        }
        for (int i = 0; i < effectComponent.Length; i++)
        {
            effectComponent[i].SetActive(riddleSolved);
        }
    }
}
