﻿using UnityEngine;
using System.Collections;
using System;

public abstract class PhisicesMover : MonoBehaviour {



	#region Automated Members
		/// <summary>
		/// The RigidBody to move.
		/// </summary>
		protected Rigidbody _rbody;
	/// <summary>
	/// The ground sensor.
	/// <para>Testing distance to ground will be done from this transform</para>
	/// <para>if not assigned directlly the transform of the current object will be used.</para>
	/// </summary>
		public Transform[] groundSensors;

    protected AudioSource audioSource;
	#endregion

	#region Public Members
	public bool decayWhenPressed;
	[Range(0,1)]
	/// <summary>
	/// The horizontal decay.
	/// <para>The horizontal velocity looses this ration </para>
	/// </summary>
	public float horizontalDecay;
	[Range(0,1)]
	/// <summary>
	/// The horizontal decay.
	/// <para>The vertical velocity looses this ration </para>
	/// </summary>
	public float verticalDecay;
	/// <summary>
	/// The forward force.
	/// </summary>
	public float forwardForce;
	/// <summary>
	/// Indicates if velocity will be hard limited.
	/// </summary>
	public bool VelocityLimit;
	/// <summary>
	/// The max horizontal velocity.
	/// </summary>
	public float maxHorizontalVelocity;
	/// <summary>
	/// The max vertical velocity.
	/// </summary>
	public float maxVerticalVelocity;
	/// <summary>
	/// The break force used ny the player to slow horizontal movment..
	/// </summary>
	public float breakForce;
	/// <summary>
	/// The horizontal velocity tollerance.
	/// </summary>
	public float horizontalVelocityTollerance;
	/// <summary>
	/// The cast mask used when raycasting to the ground.
	/// </summary>
	public  LayerMask castMask;
	/// <summary>
	/// The turn tollarance. 
	/// <para>when horizontal velocity is less then this value
	/// and a directional key is used opposite to current facing the object will turn</para>
	/// </summary>
	public float turnTollarance;
	/// <summary>
	/// The sensor offset on the y axis.
	/// <para>This should be be set over .001 to allow proper ground detection</para>
	/// </summary>
	public float sensorOffset;
    /// <summary>
    /// the sound to plpay when the 
    /// </summary>
    public AudioClip movingSound;
	#endregion

	#region Protected Members
	/// <summary>
	/// indicates if the controlled object is currentlly facing right
	/// </summary>
	protected bool facingRight;
    public bool isFacingRight
    {
        get
        {
            return facingRight;
        }
    }

	protected bool horizontalKey=false;
	#endregion

	void  Start()
	{

		init ();
	}

	protected virtual void init()
	{
		_rbody = GetComponentInParent<Rigidbody> ();
        audioSource = GetComponent<AudioSource>();
		facingRight = _rbody.gameObject.transform.right.x > 0f;
		if (groundSensors == null) 
		{
            groundSensors = new Transform[1];
			groundSensors[0] = GameObject.FindGameObjectWithTag(TagsIndex.Player).transform;
		}
	}

	/// <summary>
	/// Decaies the vertical velocity.
	/// </summary>
	protected void decayVertical()
	{
		_rbody.velocity -= (Vector3.up*decay(_rbody.velocity.y,verticalDecay));
	}

	protected void decayHorizontal()
	{
		_rbody.velocity -= (Vector3.right * decay (_rbody.velocity.x, horizontalDecay));
	}

	/// <summary>
	/// gets commands that controlls the actions of the mover.
	/// </summary>
	/// <param name="command">Command.</param>
	public abstract void getCommands (CommandMask commandMask);

	/// <summary>
	/// Decay the specified value by rate.
	/// </summary>
	/// <param name="value">the value to decay</param>
	/// <param name="rate">the decay rate. 0 indicates no decay while 1 is complete decay.</param>
	private  float decay(float value,float rate)
	{
		//Debug.Log (value + "--" + rate);
		return value * (rate);
	}
	/// <summary>
	/// Turns the rigidbody object 180 degrees on the y axix affectivlly reverwsing right and left.
	/// </summary>
	protected void turnAround()
	{
		_rbody.gameObject.transform.Rotate (0, 180, 0);

		facingRight = !facingRight;
	}
	/// <summary>
	/// Gets the gravity force on the object.
	/// </summary>
	/// <returns>The gravity force.</returns>
	public Vector3 getGravityForce()
	{
		return Physics.gravity * _rbody.mass;
	}
	/// <summary>
	/// Halts the horizontal movment if within tollerance.
	/// </summary>
	protected virtual void haltHorizontal()
	{
		if (Mathf.Abs (_rbody.velocity.x) < horizontalVelocityTollerance) 
		{
			_rbody.velocity = new Vector3(0,_rbody.velocity.y,0);
		}
	}
	/// <summary>
	/// Limits the velocity.
	/// </summary>
	protected void limitVelocity()
	{
		if (VelocityLimit) {
			//Debug.Log("Limit");
			Vector3 adjustedVelocity = _rbody.velocity;
			if(Mathf.Abs(_rbody.velocity.x) > maxHorizontalVelocity&&maxHorizontalVelocity>0f)
			{
				adjustedVelocity.x = maxHorizontalVelocity*_rbody.velocity.normalized.x;
			
			}
			if(Mathf.Abs(_rbody.velocity.y) > maxVerticalVelocity&&maxVerticalVelocity>0f)
			{
				adjustedVelocity.y = maxVerticalVelocity*_rbody.velocity.normalized.y;
				
			}
			_rbody.velocity=adjustedVelocity;
		}
	}

	/// <summary>
	/// Gets the distance to the ground. from the
	/// </summary>
	/// <returns>The distance to ground.</returns>
	protected float getDistanceToGround()
	{
		RaycastHit hit;
        float result = float.MaxValue;
        for (int i = 0; i < groundSensors.Length; i++)
        {
            if (Physics.Raycast(groundSensors[i].position + Vector3.up * sensorOffset, Vector3.down, out hit, 10f, castMask))
            {
                //Debug.DrawRay (transform.position, Vector3.down, Color.green,0.5f);
                if(hit.distance<result)
                {
                    result = hit.distance;
                }
                

            }
        }
        
		return result==float.MaxValue ? -1f : result;
	}

    protected float getLowestSensor()
    {
        float result = groundSensors[0].position.y;
        for(int i=1;i<groundSensors.Length;i++)
        {
            if(result<groundSensors[i].position.y)
            {
                result = groundSensors[i].position.y;
            }
        }
        return result;
    }

}
