﻿using UnityEngine;
using System.Collections;

public class FlyPhisicesMover : MonoBehaviour 
{

	#region Automated Members
	private Rigidbody _rbody;
	#endregion

	#region Public Members
	/// <summary>
	/// The max throttle. this should exeed gravity to allow climbing.
	/// </summary>
	public float maxThrottle;
	/// <summary>
	/// The minimum throttle.
	/// </summary>
	public float minThrottle;
	[Range (0,1)]
	/// <summary>
	/// The throttle up rate.
	/// </summary>
	public float throttleUpRate;
	[Range(0,1)]
	/// <summary>
	/// The throttle down rate.
	/// </summary>
	public float throttleDownRate;
	/// <summary>
	/// The throttle tolerance.
	/// </summary>
	public float throttleTolerance;
	/// <summary>
	/// The forward thrust.
	/// </summary>
	public float forwardThrust;
	/// <summary>
	/// The back thrust.
	/// </summary>
	public float backThrust;
	/// <summary>
	/// The horizontal decay.
	/// </summary>
	public float horizontalDecay;

	public float verticalDecay;
	#endregion

	#region Private Members
	/// <summary>
	/// The current throttle.
	/// </summary>
	public float CurrentThrottle;


	#endregion

	void Start()
	{
		_rbody = GetComponentInParent<Rigidbody> ();
		if (maxThrottle > getGravityForce()) {
			CurrentThrottle = getGravityForce();
		} 
		else 
		{
			CurrentThrottle = maxThrottle;
		}
	}

	void FixedUpdate()
	{
		#region Vertical Movment
		bool throttleUp;
		float throttleTarget=0f;
		bool hSlowing=true;
		bool vSlowing=true;
		if (Input.GetKey (KeyCode.UpArrow)) {
			throttleUp=true;
			vSlowing = false;
		} else if (Input.GetKey (KeyCode.DownArrow)) {
			throttleUp = false;
			vSlowing=false;
		} 
		else 
		{
			throttleTarget = getGravityForce();
			throttleUp = CurrentThrottle<getGravityForce();

		}

		if (throttleTarget == 0f) {
			throttleTarget = throttleUp ? maxThrottle : minThrottle;
		} 
		if (Mathf.Abs (throttleTarget - CurrentThrottle) < throttleTolerance) 
		{
			CurrentThrottle = throttleTarget;
		} 
		else 
		{
			CurrentThrottle = Mathf.Lerp (CurrentThrottle, throttleTarget, throttleUp ? throttleUpRate : throttleDownRate);
		}


		_rbody.AddForce (Vector3.up * CurrentThrottle);

		#endregion

		#region Horizontal Movment
		if(Input.GetKey(KeyCode.RightArrow))
		{
			_rbody.AddForce(transform.right*forwardThrust);
			hSlowing=false;
		}
		else if(Input.GetKey(KeyCode.LeftArrow))
		{
			_rbody.AddForce(-transform.right*backThrust);
			hSlowing=false;
		}
		#endregion

		#region Decay
		float newx = _rbody.velocity.x * (1 - horizontalDecay);
		float newy = _rbody.velocity.y * (1 - verticalDecay);
		_rbody.velocity = new Vector3 ((Mathf.Abs(newx)<throttleTolerance)&&hSlowing?0:newx,(Mathf.Abs(newy)<throttleTolerance)&&vSlowing?0:newy, 0f);
		#endregion
	}

	private float getGravityForce()
	{
		return Physics.gravity.magnitude * _rbody.mass;
	}

}
