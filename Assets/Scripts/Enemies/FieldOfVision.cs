﻿using UnityEngine;
using System.Collections;

public  class FieldOfVision : MonoBehaviour {

    [SerializeField]
    private Enemy enemy;


    public float range;

    public int apature;

    public int detectionDensity;
     
    public LayerMask visualLayers;

    public Transform[] targets;


    private LineRenderer displayRenderer;
    public float displayAngularSpeed;
    private Vector3 baseDisplayPoint;

    private Transform currentTarget;
   
    public Transform CurrentTarget
    {
        get
        {
            return currentTarget;
        }
    }

    void Awake()
    {
        displayRenderer = GetComponentInChildren<LineRenderer>();
    }

	// Use this for initialization
	void Start ()
    {
        displayRenderer.SetPosition(1, new Vector3(0, 0, range));
        baseDisplayPoint = new Vector3(0, 0, range);
        if(targets.Length==0)
        {
            targets = new Transform[1];
            targets[0] = PlayerController.instance.transform;
        }
	}
	
	// Update is called once per frame
	void Update ()
    {

        testDetection();

        updateDisplay(); 

	}

    private void testDetection()
    {
        currentTarget = null;
        for (int i = 0; i < targets.Length; i++)
        {
            float distanceToTarget = Vector3.Distance(transform.position, targets[i].position);

            float angleToTarget = Vector3.Angle(targets[i].position - transform.position, transform.forward);

            //Debug.Log(distanceToTarget);
            //Debug.Log(angleToTarget);
            if (distanceToTarget <= range && (angleToTarget <= apature * 0.5f))
            {
                RaycastHit hit;
                if (Physics.Raycast(transform.position, targets[i].position-transform.position, out hit))
                {
                    if (hit.collider.gameObject.transform.IsChildOf(targets[i]))
                    {
                        if (currentTarget == null || Vector3.Distance(currentTarget.position, targets[i].position) < distanceToTarget)
                        {
                            currentTarget = targets[i];
                            //Debug.Log("target in range");
                        }
                    }
                }
            }
        }
    }

    private void updateDisplay()
    {
        Transform rotationTransform = displayRenderer.transform;
        if (currentTarget == null)
        {
            if (!(rotationTransform.localRotation.eulerAngles.x <= apature * .5f || rotationTransform.localRotation.eulerAngles.x >= 360 - apature * .5f))
            {
                displayAngularSpeed *= -1;
            }
            rotationTransform.Rotate(Vector3.right, displayAngularSpeed);
            
        }
        else
        {
            rotationTransform.LookAt(currentTarget.position);
        }
        RaycastHit hit;
        if (Physics.Raycast(displayRenderer.transform.position, displayRenderer.transform.forward, out hit, range))
        {
            displayRenderer.SetPosition(1, baseDisplayPoint.normalized * (hit.distance)*(1/transform.lossyScale.x));
        }
        else
        {
            displayRenderer.SetPosition(1, baseDisplayPoint);
        }
    }


    void OnDrawGizmosSelected()
    {

        drawCross(transform.position + ((Quaternion.Euler(0,0 , apature * .5f) *transform.forward*range)));
        Gizmos.DrawLine(transform.position, transform.position + (Quaternion.Euler(0, 0, apature*.5f) * transform.forward * range));
        Gizmos.DrawLine(transform.position, transform.position + (Quaternion.Euler(0, 0, -apature*.5f) * transform.forward * range));
        drawCross(transform.position + ((Quaternion.Euler(0,0 , -apature * .5f) * transform.forward * range)));
        Gizmos.DrawLine(transform.position,(transform.position + transform.forward));
    }

    /// <summary>
    /// draws a cross on the inspector gui in a worldspace point
    /// </summary>
    /// <param name="point">The position in worldspace of the cross center</param>
    private void drawCross(Vector3 point)
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(point + Vector3.up * .5f, point - Vector3.up * .5f);
        Gizmos.DrawLine(point + Vector3.right * .5f, point - Vector3.right * .5f);
    }
}
