﻿using UnityEngine;
using System.Collections;
using System;
/// <summary>
/// a basic enemy class
/// </summary>
public class Enemy : Entity
{
    /// <summary>
    /// the behaviours that are active while no target is detected.
    /// </summary>
    public AIBehaviour[] idleBehaviours;
    /// <summary>
    /// the bahaviours that are active while a target is detected
    /// </summary>
    public AIBehaviour[] aggresiveBehaviurs;
    /// <summary>
    /// the enemies vision semsor
    /// </summary>
    protected FieldOfVision visionSensor;
    /// <summary>
    /// indicates if this enemy is in aggressive mode
    /// </summary>
    protected bool isAggresive = false;
    /// <summary>
    /// the death animator
    /// </summary>
    public Animator deathAnimations;

    private InventoryComponent inventory;

    protected override void Awake()
    {
        base.Awake();
        visionSensor = GetComponentInChildren<FieldOfVision>();
        inventory = GetComponent<InventoryComponent>();
    }

    void Start()
    {
        stopBehasviousrs(aggresiveBehaviurs);
        startBehasviousrs(idleBehaviours);
        registerTracker();
    }


    void Update()
    {
        if (_alive&&!GameManager.instance._lowPause)
        {
            if (isAggresive && visionSensor.CurrentTarget == null)
            {
                stopBehasviousrs(aggresiveBehaviurs);
                startBehasviousrs(idleBehaviours);
                isAggresive = false;
            }
            else if ((!isAggresive) && visionSensor.CurrentTarget != null)
            {
                stopBehasviousrs(idleBehaviours);
                startBehasviousrs(aggresiveBehaviurs);
                isAggresive = true;
            }
        }
    }

	private void stopBehasviousrs(AIBehaviour[] behaviours)
    {
        for (int i = 0; i < behaviours.Length; i++)
        {
            behaviours[i]._active=false;
        }

    }

    private void startBehasviousrs(AIBehaviour[] behaviours)
    {
        for (int i = 0; i < behaviours.Length; i++)
        {
            behaviours[i]._active = true;
            if(behaviours[i] is AttackBehaviour)
            {
                behaviours[i].target = visionSensor.CurrentTarget;
            }
        }

    }

    //void OnCollisionEnter(Collision collision)
    //{

    //}

    

    protected override IEnumerator death()
    {
        
        _alive = false;
        if (inventory != null)
        {
            inventory.Collectable = true;
        }
        stopBehasviousrs(idleBehaviours);
        stopBehasviousrs(aggresiveBehaviurs);
        visionSensor.gameObject.SetActive(false);
        unregisterTracker();
        if (deathAnimations != null)
        {
            deathAnimations.SetTrigger("Death");
        }
        invokeDeath();
        changeLayerRecursive(transform, 8);
        yield return new WaitForSeconds(1f) ;
    }

    /// <summary>
    /// registers the HP tracker to the <seealso cref="GameAttributeManager"/> of the player.
    /// </summary>
    public void registerTracker()
    {
        GameAttributeManager manager = GetComponent<GameAttributeManager>();
        if (manager != null)
        {
            manager.OnAttributeMofified += trackHp;
        }
    }
    /// <summary>
    /// registers the HP tracker to the <seealso cref="GameAttributeManager"/> of the player.
    /// </summary>
    public void unregisterTracker()
    {
        GameAttributeManager manager = GetComponent<GameAttributeManager>();
        if (manager != null)
        {
            manager.OnAttributeMofified -= trackHp;
        }
    }
}
