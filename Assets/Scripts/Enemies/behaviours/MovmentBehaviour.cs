﻿using UnityEngine;
using System.Collections;
using System;

/// <summary>
/// base class for AI behaviours chrged with miving the controlled <seealso cref="Entity"/>
/// </summary>
public abstract class MovmentBehaviour : AIBehaviour {

    /// <summary>
    /// the thruster component used for moving the controlled <seealso cref="Entity"/>
    /// </summary>
    public PhisicesMover thruster;


    /// <summary>
    /// the tolerance used when messuring if a movment destinaion has being reached.
    ///  if the distance between the destination and the current position is less then the tollerance it will considered as reached.
    /// </summary>
    public float accuraccuracyTollerance;
    /// <summary>
    /// the current movment destination in world space.
    /// </summary>
    protected Vector3 moveTo;
    /// <summary>
    /// the command mask used to send commands to the thruster.
    /// </summary>
    protected CommandMask movmentsCommands;
    

    protected virtual void Start()
    {
        movmentsCommands = new CommandMask();
    }
	

    protected void moveRight()
    {
        movmentsCommands.addCommand(UserCommands.RIGHT_THRUST);
    }

    protected void moveLeft()
    {
        movmentsCommands.addCommand(UserCommands.LEFT_THRUST);
    }

    protected void jump()
    {
        movmentsCommands.addCommand(UserCommands.JUMP);
    }

    protected override void step()
    {
        float xDeltaToTarget = moveTo.x - transform.position.x;
        float yDeltaToTarget = moveTo.y - transform.position.y;
        if (Mathf.Abs(xDeltaToTarget) > accuraccuracyTollerance)
        {
            if( xDeltaToTarget>0)
            {
                moveRight();
            }
            else
            {
                moveLeft();
            }
        }
        if(yDeltaToTarget>accuraccuracyTollerance)
        {
            jump();
        }

        thruster.getCommands(movmentsCommands);
        movmentsCommands.reset();
    }


}
