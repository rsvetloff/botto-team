﻿using UnityEngine;
using System.Collections;
using System;

public class PatrollBehaviour : MovmentBehaviour {

    public Vector3[] patrolPoints;


    private int currentWaypoint;

    private bool onReturn;

    protected override void Start()
    {
        base.Start();
        moveTo = patrolPoints[0];
    }

    protected override void step()
    {
        if(Vector3.Distance(transform.position,patrolPoints[currentWaypoint])<=accuraccuracyTollerance)
        {
            moveTo = getNextWaypoint();
        }
        base.step();
    }

    private Vector3 getNextWaypoint()
    {
        if (currentWaypoint>= patrolPoints.Length-1)
        {
            onReturn = true;
        }
        else if (currentWaypoint<=0)
        {
            onReturn = false;
        }

        if (onReturn)
        {
            currentWaypoint--;
        }
        else
        {
            currentWaypoint++;
        }
        return patrolPoints[currentWaypoint];

    }


    private int getClosestWaypointIndex()
    {
        int result = -1;
        float shortestDistance=-1f;
        for(int i=0;i<patrolPoints.Length;i++)
        {
            float distance = Vector3.Distance(transform.position, patrolPoints[i]);
            if (distance<shortestDistance)
            {
                shortestDistance = distance;
                result = i;
            }
        }
        return result;
    }

   
}
