﻿using UnityEngine;
using System.Collections;
using System;

public class AttackBehaviour : AIBehaviour {

    public Weapon[] _weapons;

    private CommandMask commands;

    /// <summary>
    /// the transforms that will point twards the target;
    /// </summary>
    public Transform[] pointers;

    protected void Start()
    {
        commands = new CommandMask();
    }

    protected override void step()
    {
        for (int j = 0; j < _weapons.Length; j++)
        {
            Weapon _weapon = _weapons[j];
            if (_weapon.CurrentAmmo <= 0)
            {
                commands.addCommand(UserCommands.RELOAD);
            }
            if (target != null && Vector3.Distance(transform.position, target.position) <= _weapon.range)
            {
                //_weapon.transform.LookAt(target);
                for (int i = 0; i < pointers.Length; i++)
                {
                    pointers[i].LookAt(target);
                }
                commands.addCommand(UserCommands.FIRE);
            }
            _weapon.getCommands(commands);
        }
    }

    
}
