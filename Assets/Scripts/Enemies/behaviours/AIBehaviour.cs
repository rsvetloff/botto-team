﻿using UnityEngine;
using System.Collections;

public abstract class AIBehaviour : MonoBehaviour {

    public Transform target;
    /// <summary>
    /// indicates that the behaviour is active and effect the target
    /// </summary>
    public bool _active;


    public bool IsActive
    {
        get
        {
            return _active;
        }
    }

    [SerializeField]
    private bool _useFixedUpdate;

	// Update is called once per frame
	protected virtual void Update ()
    {
        if(_active && !_useFixedUpdate && !GameManager.instance._lowPause)
        {
            step();
        }
	}

    protected virtual void FixedUpdate()
    {
        if (_active && _useFixedUpdate && !GameManager.instance._lowPause)
        {
            step();
        }
    }


    protected abstract void step();

}
