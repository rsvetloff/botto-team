﻿using UnityEngine;
using System.Collections;

public abstract class RiddleResponder : MonoBehaviour
{

    public abstract void respond(bool riddleSolved);

}
