﻿using UnityEngine;
using System.Collections;
using System;

public class KillRiddle : RiddleManager
{
    /// <summary>
    /// monotired kill targets
    public Entity[] targets;
    /// </summary>
   

    public bool testForAlive;

    public void Start()
    {
        for(int i=0;i<targets.Length;i++)
        {
            targets[i].OnDeath += checkStatus;
        }
    }

    public override void checkStatus()
    {
        bool result = true;
        for (int i=0;i<targets.Length;i++)
        {
            if(targets[i].Alive != testForAlive)
            {
                result = false;
                break;
            }
        }
        if(result)
        {
            riddleSolved();
        }
    }

    protected override void resetRiddle()
    {
        
    }
}
