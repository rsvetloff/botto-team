﻿using UnityEngine;
using System.Collections;
using System;

public class ActivationResponder : RiddleResponder
{
    public bool testTrue;

    public override void respond(bool riddleSolved)
    {
        gameObject.SetActive(riddleSolved == testTrue);
    }
}
