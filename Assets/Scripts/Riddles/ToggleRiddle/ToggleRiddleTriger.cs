﻿using UnityEngine;
using System.Collections;
using System;

public class ToggleRiddleTriger : RiddleTriger
{
    private bool entered=false;
    public Transform visual;
    public GameObject activationPopup;

    protected override void trigerActivation()
    {
        RiddleAction[] currentStage = getNextStage();
        
        
        for (int i = 0; i < currentStage.Length; i++)
        {
            currentStage[i].toggled();
        }
        nextStage++;
        visual.transform.Rotate(0, 0, 360 / (numOfStages * 2));
        manager.checkStatus();
        
        
    }

    public override void restTrigger()
    {
        base.restTrigger();
        visual.Rotate(0, 0, -visual.rotation.eulerAngles.z);
    }

   

    protected override void OnTriggerEnter(Collider other)
    {
        registerActivation(other.gameObject);
        
    }

    protected override void OnTriggerExit(Collider other)
    {
        unregisterActivation(other.gameObject);
    }

    private void registerActivation(GameObject triggeringObject)
    {
        if (triggeringObject.tag.Equals(TagsIndex.Player))
        {
            if (!entered)
            {

                PlayerController.playerInteraction += trigerActivation;
                //Debug.Log("Enter:" + gameObject.name);
                entered = true;
                togglePopup(true);
            }
        }
    }

    private void unregisterActivation(GameObject triggeringObject)
    {
        if (triggeringObject.tag.Equals(TagsIndex.Player))
        {
            PlayerController.playerInteraction -= trigerActivation;
            //Debug.Log("exit:" + gameObject.name);
            entered = false;
            togglePopup(false);
        }
    }

    private void togglePopup(bool newState)
    {
        activationPopup.SetActive(newState);
    }
}
