﻿using UnityEngine;
using System.Collections;

public abstract class RiddleAction : MonoBehaviour {

    public int currentState;
    public bool toggleState;

    public abstract void toggled();
}
