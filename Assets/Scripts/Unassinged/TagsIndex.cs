﻿using UnityEngine;
using System.Collections;

public static class TagsIndex  
{
	public const string Player= "Player";
    public const string Weapon = "Weapon";
    public const string GameManager = "GameController";
    public const string LevelManager = "LevelManager";
    public const string LevelPhase = "LevelPhase";
}
