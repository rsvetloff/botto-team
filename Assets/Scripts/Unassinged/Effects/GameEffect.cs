﻿using UnityEngine;
using System.Collections;

public class GameEffect : MonoBehaviour 
{

#if UNITY_EDITOR
	public GameAttributesSet getAffectedAttributes()
	{
		return _affectedAttributes;
	}

#endif



	/// <summary>
	/// The attributes affected by this effect.
	/// </summary>
	protected GameAttributesSet _affectedAttributes;

	/// <summary>
	/// indicates tat the effect is active.
	/// </summary>
	protected bool _active = true;


	protected GameAttributeManager _affectedManager;
	

	/// <summary>
	/// applies the effect to the attribute manager.
	/// </summary>
	/// <param name="affectedManager">Affected manager.</param>
	public void activate(GameAttributeManager affectedManager)
	{
		_active = true;
		_affectedManager = affectedManager;

	}

	/// <summary>
	/// Deactivate this instance.
	/// </summary>
	public void deactivate(GameAttributeManager affectedManager)
	{
		_active = false;
	}


}