﻿using UnityEngine;
using System.Collections;

public class TimmedGameEffect : GameEffect {

	/// <summary>
	/// The effect timer.
	/// </summary>
	public float effectTimer;


	

	void Update () 
	{
		if (_active) 
		{
			effectTimer-=Time.deltaTime;
			if(effectTimer<=0)
			{
				//deactivate();
			}
		}
	}
}
