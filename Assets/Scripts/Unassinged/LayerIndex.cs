﻿using UnityEngine;
using System.Collections;

public enum LayerIndex
{
    Default=0,
    TransparentFX= 1<<1,
    Ignore_Raycast = 1<<2,
    Layer3=1<<3,
    Water = 1<<4,
    UI = 1<<5,
    Layer6 = 1 << 6,
    Layer7 = 1 << 7,
    Ground = 1<<8,
    Reference=1<<9,
    Notification=1<<10

}
