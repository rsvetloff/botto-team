﻿using UnityEngine;
using System.Collections;

public class CrushDetector : MonoBehaviour {

    private Entity _entity;

    private void Awake()
    {
        _entity = GetComponentInParent<Entity>();
    }


	void OnTriggerEnter(Collider other)
    {
        _entity.raiseDeath();
    }
}
