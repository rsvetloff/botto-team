﻿using UnityEngine;
using System.Collections;

public class AimController : MonoBehaviour {

	#region public Members
	public Sprite crosshair;

	public Camera viewCamera;



	#endregion

	#region inspector members
	[SerializeField]
	private float pointerSpeed;
	[SerializeField]
	/// <summary>
	/// The max angle the aim can reach up.
	/// </summary>
	private float maxAngle;
	[SerializeField]
	/// <summary>
	/// The minimum angle the aim can reach down
	/// </summary>
	private float minAngle;


	#endregion

	// Use this for initialization
	void Awake () {
        if(viewCamera==null)
        {
            viewCamera = Camera.main;
        }
	}

    public Vector3 getMouseWorldPosition()
    {
        Ray ray = viewCamera.ScreenPointToRay(Input.mousePosition);
        Debug.DrawLine(ray.origin, ray.origin - (ray.direction * viewCamera.transform.position.z));
        Vector3 target = ray.origin - (ray.direction * viewCamera.transform.position.z);
        target.z = 0f;
        return target;
    }

    
	
	// Update is called once per frame
	void Update () 
	{
        //viewCamera.ScreenPointToRay (Input.mousePosition);
        if (!GameManager.instance._lowPause)
        {
            transform.LookAt(getMouseWorldPosition(), transform.up);

            transform.Rotate(0, 0, -transform.rotation.eulerAngles.z);
        }
    }
}
