﻿using UnityEngine;
using System.Collections;

public class RobotBuilder : MonoBehaviour {


    /// <summary>
    /// the wraper in to which the robot weapon is inserted
    /// </summary>
    public GameObject WeaponWraper;
    /// <summary>
    /// the wraper in to which the robot passive is inserted
    /// </summary>
    public GameObject passiveWraper;
    /// <summary>
    /// the wraper in to which the robot thruster is inserted
    /// </summary>
    public GameObject thrustWraper;
    /// <summary>
    /// the currentlly equiped weapon
    /// </summary>
    public static ItemIndex currentWeapon;
    /// <summary>
    /// currently equiped pasiive.
    /// </summary>
    public static ItemIndex currentPassive;
    /// <summary>
    /// currently equiped thruster
    /// </summary>
    public static ItemIndex currentThruster;

    void Awake()
    {
        
    }

    public void buildRobot(InventoryItem activeWeapon, InventoryItem activePassive, InventoryItem activeThrust)
    {
        GameObject addition;
        
        if (activeWeapon != null  && activeWeapon._itemCode!=currentWeapon)
        {
            currentWeapon = activeWeapon._itemCode;
            delChildren(WeaponWraper.transform);
            addition = Instantiate(activeWeapon._prefab);
            Vector3 newPos = addition.transform.position;
            Vector3 newScale = addition.transform.lossyScale;
            Quaternion newRot = addition.transform.rotation;
            addition.transform.SetParent(WeaponWraper.transform);
            Weapon[] addedWeapons = GetComponentsInChildren<Weapon>();
            if(addedWeapons!=null)
            {
                for(int i=0;i<addedWeapons.Length;i++)
                {
                    addedWeapons[i].initilize();
                }
            }
            addition.transform.localPosition = newPos;
            addition.transform.localScale = newScale;
            addition.transform.localRotation = newRot;
        }
        if (activePassive != null && activePassive._itemCode!=currentPassive)
        {
            currentPassive = activePassive._itemCode;
            delChildren(passiveWraper.transform);
            addition = Instantiate(activePassive._prefab);
            Vector3 newPos = addition.transform.position;
            Vector3 newScale = addition.transform.lossyScale;
            Quaternion newRot = addition.transform.rotation;
            addition.transform.SetParent(passiveWraper.transform);
            addition.transform.localPosition = newPos;
            addition.transform.localScale = newScale;
            addition.transform.localRotation = newRot;


        }
        if (activeThrust != null && activeThrust._itemCode!= currentThruster)
        {
            currentThruster = activeThrust._itemCode;
            delChildren(thrustWraper.transform);
            addition = Instantiate(activeThrust._prefab);
            Vector3 newPos = addition.transform.position;
            Vector3 newScale = addition.transform.lossyScale;
            Quaternion newRot = addition.transform.rotation;
            addition.transform.SetParent(thrustWraper.transform);
            addition.transform.localPosition = newPos;
            addition.transform.localScale = newScale;
            addition.transform.localRotation = newRot;

        }
        PlayerController.instance.getParts();
        transform.position = new Vector3(LevelManager.ActiveLevel.ActivePrinter.transform.position.x, LevelManager.ActiveLevel.ActivePrinter.transform.position.y+1, transform.position.z);
    }

    public void buildRobot(InventoryItem item)
    {

        switch (item._type)
        {
            case InventoryItemBase.InventoryItemTypes.General:
                break;
            case InventoryItemBase.InventoryItemTypes.Thruster:
                buildRobot(null, null, item);
                break;
            case InventoryItemBase.InventoryItemTypes.Weapon:
            buildRobot(item, null, null);
            break;

        }
    }

    private void delChildren(Transform target)
    {
        GameObject[] delTargets = new GameObject[target.childCount];
        for(int i=0;i<delTargets.Length;i++)
        {
            delTargets[i] = target.GetChild(i).gameObject;
        }
        for (int i = 0; i < delTargets.Length; i++)
        {
            delTargets[i].SetActive(false);
            Destroy(delTargets[i]);
        }
    }
}
