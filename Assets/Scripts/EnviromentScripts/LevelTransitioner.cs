﻿using UnityEngine;
using System.Collections;

public class LevelTransitioner : MonoBehaviour {
    /// <summary>
    /// the scene to transition to.
    /// </summary>
	public string targetScene;

    void OnTriggerEnter (Collider other)
    {
        PlayerController player = other.GetComponentInParent<PlayerController>();
        if(player!=null && gameObject.activeInHierarchy)
        {
            player.enabled = false;
            GameManager.instance._lowPause = true;
            LevelManager.ActiveLevel.switchScene(targetScene);
        }
    }
}
