﻿using UnityEngine;
using System.Collections;

public class CameraMovmentSync : MonoBehaviour {

    public float horizontalTrackFactor;
    public float verticalTrackFactor;

    public Transform tralingCamera;

    private Vector3 lastCameraPosotion;

    void Awake()
    {
        if (tralingCamera== null)
        {
            tralingCamera = Camera.main.transform;
        }
        lastCameraPosotion = tralingCamera.position;
    }

    // Update is called once per frame
    void Update ()
    {
	    if(!lastCameraPosotion.Equals(tralingCamera.position))
        {
            transform.Translate((tralingCamera.position.x - lastCameraPosotion.x) * horizontalTrackFactor, (tralingCamera.position.y - lastCameraPosotion.y) * verticalTrackFactor, 0f);
            lastCameraPosotion = tralingCamera.position;
        }
	}
}
