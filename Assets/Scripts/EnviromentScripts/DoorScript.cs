﻿using UnityEngine;
using System.Collections;

public class DoorScript : MonoBehaviour {

	private Animator anim;
	private Transform player;


	public float TriggerDistance;
	public bool leftTrigger;
	public bool rightTrigger;

	public bool disableOnClose;

	private bool open;

	void Start()
	{
		anim = GetComponent<Animator> ();
		player = GameObject.FindGameObjectWithTag (TagsIndex.Player).transform;
	}
	void Update()
	{
		if (Mathf.Abs(transform.position.x-player.position.x) <= TriggerDistance) {
			if (!open&&(
			    (leftTrigger&&player.position.x<=transform.position.x)|| 
			    (rightTrigger&&player.position.x>transform.position.x)))
			{
				anim.SetTrigger ("DoOpen");
				open = true;
			}
		} 
		else 
		{
			if(open)
			{
				anim.SetTrigger ("DoClose");
				open = false;
			}
		}
	}
}
