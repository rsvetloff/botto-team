﻿using UnityEngine;
using System.Collections;

public class PlatformRider : MonoBehaviour {

    [HideInInspector]
    private Transform father;
    /// <summary>
    /// the transform that is caried by the platform
    /// </summary>
    public Transform affectedTransform;


    void Start()
    {
        father = affectedTransform.parent;
       // Debug.Log("parent:" + father.name);
    }
    
    public void transferParent(Transform newParent)
    {
        
        affectedTransform.parent = newParent;
    }

   

   

    public void restParent()
    {
        affectedTransform.parent = father;
    }

}
