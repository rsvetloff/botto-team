﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Collider))]
public class PlatformTrigger : MonoBehaviour {

    /// <summary>
    /// the triger that starts the collection
    /// </summary>
    private Collider _trigger;
    /// <summary>
    /// thew controlling script of the platform
    /// </summary>
    private PlatformController _platformController;
    
    void Awake()
    {
        _platformController = GetComponentInParent<PlatformController>();
    }

	// Use this for initialization
	void Start ()
    {
        _trigger = GetComponent < Collider > ();
        _trigger.isTrigger = true;
	}


    void OnTriggerEnter(Collider other)
    {
        PlatformRider rider = getRider(other);
        //Debug.Log("platform:on");
        if(rider!=null)
        {
            //rider.transferParent(GetComponentInParent<Rigidbody>());
            rider.transferParent(_platformController.affectedFather);
        }
    }


    void OnTriggerExit(Collider other)
    {
        PlatformRider rider = getRider(other);
        //Debug.Log("platform:off");
        if (rider!=null)
        {
            rider.restParent();
        }
    }
	

    private PlatformRider getRider(Collider riderCollider)
    {
        return riderCollider.gameObject.GetComponentInParent<PlatformRider>();
    }
	
}
