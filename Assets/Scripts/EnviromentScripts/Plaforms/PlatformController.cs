﻿using UnityEngine;
using System.Collections;


/// <summary>
/// Controller for moving platforms in the game
/// </summary>
public class PlatformController : MonoBehaviour {

    /// <summary>
    /// the movment velocity of the platform.
    /// if way points are used this vector size will determine the speed of movment
    /// </summary>
    public Vector3 velocity;

    /// <summary>
    /// waypoints to move throught.
    /// </summary>
    public Vector3[] wayPoints;

    public float[] waypointDelay;


    public float WaypointTollerance;

    public float easeDistance;
    public float easeRatio;
    /// <summary>
    /// determines if the platform will move
    /// </summary>
    public bool Playing;
    /// <summary>
    /// determine if the waypoints will loop back when the last one is reached.
    /// </summary>
    public bool loopWaypoints = true;
    /// <summary>
    /// The triger that collects  passengers
    /// </summary>
    public Collider collectionTrigger;

    /// <summary>
    /// the rigidBody that preforms the 
    /// </summary>
    public  Rigidbody simulationBody;

    /// <summary>
    /// the father of the elemnts affected by the simulationBody actions
    /// </summary>
    [Tooltip("the transform affented by the simmulation body")]
    public Transform affectedFather;

    public float timer = 0;

    public int nextWayPoint=0;

    public AudioClip crashSound;

    


    private const float crossRadius = .5f;

    void Awake()
    {
        if (simulationBody == null)
        {
            simulationBody = GetComponentInChildren<Rigidbody>();
        }
    }
	
    void Update()
    {
        if(timer>0f)
        {
            timer -= Time.deltaTime;
            
        }

        //affectedFather.transform.position = simulationBody.transform.position;
    }

	void FixedUpdate ()
    {
        if (Playing && !GameManager.instance._lowPause)
        {
            movePlatform();
        }
        
	}

    /// <summary>
    /// Moves the platform.
    /// </summary>
    private void movePlatform()
    {
        affectedFather.transform.position = simulationBody.transform.position;
        if (wayPoints.Length == 0)
        {
            simulationBody.MovePosition(simulationBody.transform.position +( velocity * Time.fixedDeltaTime));
        }
        else
        {
            //Debug.Log(PlayerController.instance.GetComponent<Rigidbody>().velocity);
            if(Vector3.Distance(simulationBody.transform.position,wayPoints[nextWayPoint%wayPoints.Length])<WaypointTollerance)//waypoint reached
            {
                nextWayPoint++;
                if(nextWayPoint%wayPoints.Length==-0)//looping check
                {
                    Playing = loopWaypoints;
                }
                if(nextWayPoint%wayPoints.Length<waypointDelay.Length)
                {
                    float nextDelay = waypointDelay[nextWayPoint % wayPoints.Length];
                    if(nextDelay<0)
                    {
                        Playing = false;
                    }
                    else
                    {
                        timer = nextDelay;
                    }
                }
            }
            else
            {
                if (timer <= 0f)
                {
                    Vector3 movmentVector = (wayPoints[nextWayPoint % wayPoints.Length] -simulationBody.transform.position).normalized * velocity.magnitude* Time.fixedDeltaTime;
                    /*if (Vector3.Distance(transform.position, wayPoints[nextWayPoint % wayPoints.Length]) < movmentVector.magnitude)
                    {
                        _rbody.MovePosition(  wayPoints[nextWayPoint % wayPoints.Length]);
                    }
                    else
                    {*/
                        float distanceToTarget= Vector3.Distance(simulationBody.transform.position, wayPoints[nextWayPoint % wayPoints.Length]);
                        if (distanceToTarget > easeDistance)
                        {
                            distanceToTarget = Vector3.Distance(simulationBody.transform.position, wayPoints[Mathf.Abs((nextWayPoint - 1) % wayPoints.Length)]);
                            if(distanceToTarget>easeDistance)
                            {
                                distanceToTarget = -1f;
                            }
                        }
                        if(distanceToTarget>0)
                        {
                        //Debug.Log("ease");
                            movmentVector *= distanceToTarget / easeDistance;
                        }
                        simulationBody.MovePosition(simulationBody.transform.position+ movmentVector);
                    //}
                }
            }
        }
    }




    void OnDrawGizmosSelected()
    {
        if (wayPoints != null)
        {
            for(int i=0;i<wayPoints.Length;i++)
            {
                drawCross(wayPoints[i]);
            }
        }

    }

    /// <summary>
    /// draws a cross on the inspector gui in a worldspace point
    /// </summary>
    /// <param name="point">The position in worldspace of the cross center</param>
    private void drawCross(Vector3 point)
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(point + Vector3.up * crossRadius, point - Vector3.up * crossRadius);
        Gizmos.DrawLine(point + Vector3.right * crossRadius, point - Vector3.right * crossRadius);
    }

}
