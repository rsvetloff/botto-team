﻿using UnityEngine;
using System.Collections;
using System;

public class PlatformSwitch : MonoBehaviour {

    private bool entered = false;
    public GameObject activationPopup;

    public PlatformController platform;

    public AudioSource buttonPressedAudio;

     void OnTriggerEnter(Collider other)
    {
        PlayerController player = other.GetComponentInParent<PlayerController>();
        if (enabled && !entered && player!=null)
        {
            PlayerController.playerInteraction += trigerActivation;
            //Debug.Log("Enter:" + gameObject.name);

            togglePopup(true);
            entered = true;
        }
    }


    void OnTriggerExit(Collider other)
    {
        PlayerController.playerInteraction -= trigerActivation;
        //Debug.Log("exit:" + gameObject.name);
        entered = false;
        togglePopup(false);
    }

    private void trigerActivation()
    {
        if (enabled)
        {
            buttonPressedAudio.Play();
            platform.Playing = true;
        }
    }

    void Update()
    {

    }

    private void togglePopup(bool newState)
    {
        activationPopup.SetActive(newState);
    }
}
