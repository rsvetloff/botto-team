﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// represents a timmer activates trigger effect 
/// </summary>
public class TimmedBarrier : MonoBehaviour
{
    /// <summary>
    /// the wraper containing the trigers and viasuals.
    /// </summary>
    public GameObject triggerWraper;

    /// <summary>
    /// represents the the timmer is currently counting.
    /// </summary>
    public bool counting;
    /// <summary>
    /// the delay between activation cycles.
    /// </summary>
    public float delay;
    /// <summary>
    /// thetimes per second the effects will trigger
    /// </summary>
  //  public float rateOfEffect;
    /// <summary>
    /// the <seealso cref="HitEffect"/> that will be applied to affected objects
    /// </summary>
    private HitEffect[] barrierEffects;
    /// <summary>
    /// the bodies that will recive effects from the barrier
    /// </summary>
    private List<Rigidbody> affectedBodies;
    /// <summary>
    /// the time counter for the activation cycles
    /// </summary>
   private float timmer;
   

    void Start()
    {
        affectedBodies = new List<Rigidbody>();
        barrierEffects = GetComponents<HitEffect>();
    }

    void Update()
    {
        advanceCount();
    }
    /// <summary>
    /// advances the timer and toggles as needed
    /// </summary>
    private void advanceCount()
    {
        if (counting)
        {
            timmer += Time.deltaTime;
            if (timmer > delay)
            {
                toggleState();
                timmer = 0f;
            }
        }
    }


    void OnTriggerEnter(Collider other)
    {
        Rigidbody otherBody = other.GetComponentInParent<Rigidbody>();   
        if(otherBody !=null && !affectedBodies.Contains(otherBody))
        {
            affectedBodies.Add(otherBody);
            applyEffect(otherBody.gameObject);
        }
    }

    void OnTriggerExit(Collider other)
    {
        Rigidbody otherBody = other.GetComponentInParent<Rigidbody>();
        if(otherBody!=null)
        {
            affectedBodies.Remove(otherBody);
        }
    }

    /// <summary>
    /// applies the effects to all registered bodies
    /// </summary>
    private void applyEffects()
    {
        for (int i=0;i<affectedBodies.Count;i++)
        {
            applyEffect(affectedBodies[i].gameObject);
        }
    }

    private void applyEffect(GameObject target)
    {
        for (int j = 0; j < barrierEffects.Length; j++)
        {
            barrierEffects[j].applyEffect(target);
        }
    }

    private void toggleState()
    {
        
        triggerWraper.SetActive(!triggerWraper.activeSelf);
    }
}
