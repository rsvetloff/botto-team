﻿using UnityEngine;
using System.Collections;

public class PrinterTriggerScript : MonoBehaviour {
    /// <summary>
    /// indicates the printers' trigger was entered but not exited 
    /// </summary>
    private bool entered = false;
    /// <summary>
    /// the popuyp indicator for the printer
    /// </summary>
    public GameObject activationPopup;
    /// <summary>
    /// the camera that wathes the printer for the printers menu.
    /// </summary>
    public Camera uiCamera;
    /// <summary>
    /// the name of the printer to identify it in the ui.
    /// </summary>
    public string printerName;
    /// <summary>
    /// the position at which the robot will appear when the priter builds.
    /// </summary>
    public Transform output;
    /// <summary>
    /// a tutorial page for the printer.
    /// </summary>
    [SerializeField]
    private TutorialPageManager printerTutorial;

    private void trigerActivation()
    {
        LevelManager.ActiveLevel.activatePrinter(this,true);
        if(printerTutorial!=null)
        {
            printerTutorial.show();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        registerActivation(other.gameObject);

    }

    private void OnTriggerExit(Collider other)
    {
        unregisterActivation(other.gameObject);
    }

    private void registerActivation(GameObject triggeringObject)
    {
        if (triggeringObject.tag.Equals(TagsIndex.Player))
        {
            if (!entered)
            {

                PlayerController.playerInteraction += trigerActivation;
                //Debug.Log("Enter:" + gameObject.name);
                entered = true;
                togglePopup( true);
            }
        }
    }

    private void unregisterActivation(GameObject triggeringObject)
    {
        if (triggeringObject.tag.Equals(TagsIndex.Player))
        {
            PlayerController.playerInteraction -= trigerActivation;
            //Debug.Log("exit:" + gameObject.name);
            entered = false;
            togglePopup( false);
        }
    }

    private void togglePopup(bool newState)
    {
        activationPopup.SetActive(newState);
     }

}