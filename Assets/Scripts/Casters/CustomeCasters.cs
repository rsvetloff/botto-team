﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// this class provides custom raycast shapes.
/// </summary>
public static class CustomeCasters 
{
	#region Cone Casters
	/// <summary>
	/// casting a cone shape and returns true if a hit was detected.
	/// </summary>
	/// <returns><c>true</c>, if cast was coned, <c>false</c> otherwise.</returns>
	/// <param name="origin">thr origin point iof the cone.</param>
	/// <param name="direction">the direction of the cone.</param>
	/// <param name="openAngle">The opening of the cone angle.</param>
	/// <param name="length">The length of con's central span.</param>
	/// <param name="density">The Density of rays to use for detection.
	/// <para>a value of 0 means only the edges of the cone will be cast.</para> </param>
	/// <param name="hit">Information about the hit object. the first hit result will be returned</para>.</param>
	public static bool ConeCast2D(Vector3 origin,
	                            Vector3 direction,
	                            int openAngle,
	                            float length,
	                            int density,
	                            out RaycastHit hit)
	{
		return ConeCast2D (origin, direction, openAngle, length, density,out hit, int.MaxValue);


	}

	/// <summary>
	/// casting a cone shape and returns true if a hit was detected.
	/// </summary>
	/// <returns><c>true</c>, if cast was coned, <c>false</c> otherwise.</returns>
	/// <param name="origin">thr origin point iof the cone.</param>
	/// <param name="direction">the direction of the cone.</param>
	/// <param name="openAngle">The opening of the cone angle.</param>
	/// <param name="length">The length of con's central span.</param>
	/// <param name="density">The Density of rays to use for detection.
	/// <para>a value of 0 means only the edges of the cone will be cast.</para> </param>
	/// <param name="hit">Information about the hit object. the first hit result will be returned</para>.</param>
	/// <param name="targetLayers"></para>
	public static bool ConeCast2D(Vector3 origin,
	                              Vector3 direction,
	                              int openAngle,
	                              float length,
	                              int density,
	                              out RaycastHit hit,
	                              LayerMask targetLayers)
	{
		Vector3 directional = direction.normalized*length;
		float step = openAngle/(density>=0?density+1:density);
		RaycastHit result;
		directional = Quaternion.Euler (0, 0, (openAngle % 360) / 2) * directional;
		for (int i =0; i<density+2; i++) 
		{
			Debug.DrawRay(origin,directional,Color.cyan,2,true);
			if(Physics.Raycast(origin,directional,out result,length,targetLayers))
			{
				hit = result;

				return true;
			}
			directional = Quaternion.Euler(0,0,-step)*directional;
		}
		hit = new RaycastHit ();
		return false;
	}

    /// <summary>
	/// casting a cone shape and returns all hits made by the forming rays
	/// </summary>
	/// <returns>all hits made by the conens forming rays</returns>
	/// <param name="origin">thr origin point iof the cone.</param>
	/// <param name="direction">the direction of the cone.</param>
	/// <param name="openAngle">The opening of the cone angle.</param>
	/// <param name="length">The length of con's central span.</param>
	/// <param name="density">The Density of rays to use for detection.
	/// <para>a value of 0 means only the edges of the cone will be cast.</para> </param>
	/// <param name="hit">Information about the hit object. the first hit result will be returned</para>.</param>
	/// <param name="targetLayers"></para>
	public static GameObject[] ConeCast2D(Vector3 origin,
                                  Vector3 direction,
                                  int openAngle,
                                  float length,
                                  int density,
                                  LayerMask targetLayers)
    {
        Vector3 directional = direction.normalized * length;
        float step = openAngle / (density >= 0 ? density + 1 : density);
        List<GameObject> hits = new List<GameObject>();
        RaycastHit result;
        directional = Quaternion.Euler(0, 0, (openAngle % 360) / 2) * directional;
        for (int i = 0; i < density + 2; i++)
        {
            Debug.DrawRay(origin, directional, Color.cyan, 2, true);
            if (Physics.Raycast(origin, directional, out result, length, targetLayers))
            {
                hits.Add(result.collider.gameObject);
            }
            directional = Quaternion.Euler(0, 0, -step) * directional;
        }

        return hits.Distinct().ToArray() ;
    }

    /// <summary>
    /// casts a rounded cone and returns all hits detected.
    /// </summary>
    /// <returns>all hits detected within the cone</returns>
    /// <param name="origin">the point of the cone.</param>
    /// <param name="direction">The direction that the cone's central span point.</param>
    /// <param name="openAngle">Open angle. the angle of cone's opening</param>
    /// <param name="length">Length. the length of the central span of the cone</param>
    /// <param name="density">Density. The number of rays to use in testing the cone</param>
    /// <param name="targetLayers">The layers to cast to</param>
    public static RaycastHit[] ConeCastAll2D(Vector3 origin,
	                              Vector3 direction,
	                              int openAngle,
	                              float length,
	                              int density,
	                              LayerMask targetLayers)
	{
		Vector3 directional = direction.normalized*length;
		float step = openAngle/(density>=0?density+1:density);
		List<RaycastHit> result = new List<RaycastHit> ();

		directional = Quaternion.Euler (0, 0, (openAngle % 360) / 2) * directional;
		for (int i =0; i<density+2; i++) 
		{
			result.AddRange(Physics.RaycastAll(origin,directional,length,targetLayers));
			Debug.DrawRay(origin,directional,Color.cyan,2,false);
			directional = Quaternion.Euler(0,0,-step)*directional;
		}

		return result.Distinct ().ToArray ();

	}

	/// <summary>
	/// casts a rounded cone and returns all hits detected.
	/// </summary>
	/// <returns>all hits detected within the cone</returns>
	/// <param name="origin">the point of the cone.</param>
	/// <param name="direction">The direction that the cone's central span point.</param>
	/// <param name="openAngle">Open angle. the angle of cone's opening</param>
	/// <param name="length">Length. the length of the central span of the cone</param>
	/// <param name="density">Density. The number of rays to use in testing the cone</param>
	public static RaycastHit[] ConeCastAll2D(Vector3 origin,
	                                         Vector3 direction,
	                                         int openAngle,
	                                         float length,
	                                         int density)
	{
		return ConeCastAll2D (origin, direction, openAngle, length, density, int.MaxValue);
		
	}

	#endregion
}
