﻿using UnityEngine;
using System.Collections;
public delegate void eventHandler();


public abstract class Entity : MonoBehaviour
{

    

    public event eventHandler OnDeath;

    public AudioClip deathSound;

    protected AudioSource audioSource;

    protected bool _alive=true;

    protected virtual void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }
    /// <summary>
    /// indicates if the player is alive.
    /// </summary>
    public bool Alive
    {
        get
        {
            return _alive;
        }
    }

    /// <summary>
    /// recevies changes in the players <seealso cref="GameAttributeManager"/> and tracks current HP to detect death
    /// </summary>
    /// <param name="change"></param>
    protected void trackHp(AttributeChange change)
    {
        if (change.ModifiedAttribute == GameAttributeCodes.CURRENT_HP && change.NewValue <= 0f)
        {
            raiseDeath();
        }

    }

    public void raiseDeath()
    {
        _alive = false;
        audioSource.clip = deathSound;
        audioSource.loop = false;
        audioSource.spatialBlend = 0;
        audioSource.Play();
        StartCoroutine(death());
    }

    protected void invokeDeath()
    {
        if (OnDeath != null)
        {
            OnDeath.Invoke();
        }
    }
    protected abstract IEnumerator death();

    /// <summary>
    /// recursavlly modifies the layer of an object and all its children
    /// </summary>
    /// <param name="target">the object to change</param>
    /// <param name="layer">the bew kayer to apply.</param>
    protected void changeLayerRecursive(Transform target,int layer)
    {
        target.gameObject.layer = layer;
        for(int i=0;i<target.childCount;i++)
        {
            changeLayerRecursive(target.GetChild(i), layer);
        }
    }
}
