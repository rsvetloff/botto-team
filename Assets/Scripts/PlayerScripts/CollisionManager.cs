﻿using UnityEngine;
using System.Collections.Generic;
/// <summary>
/// test if the entity is crushed between two colliders and if so calls death.
/// </summary>
public class CollisionManager : MonoBehaviour
{
    /// <summary>
    /// containes all current collisions on the entity
    /// </summary>
    private Dictionary<Collider,Collision> _collisions;
    /// <summary>
    /// the tracked entity
    /// </summary>
    private Entity _entity;
    /// <summary>
    /// the minimum relative angle between two collision required to achive a crush.
    /// </summary>
    public float angleTolerance=135;

    public LayerMask crushingLayers;

    void Awake()
    {
        _entity = GetComponent<Entity>();
    }

    void Start()
    {
        _collisions = new Dictionary<Collider, Collision>();
    }

    void OnCollisionEnter(Collision coll)
    {
        //if (_collisions.ContainsKey(null))
        //{
        //    _collisions.Remove(null);
        //}
        if (!GameManager.instance._lowPause)
        {
            crushTest(coll);
        }
        if (_collisions.ContainsKey(coll.collider))
        {
            _collisions[coll.collider] = coll;
        }
        else
        {
            _collisions.Add(coll.collider, coll);
        }
        //Debug.Log("add colission to:" + _collisions.Count);
    }

    void OnCollisionStay(Collision col)
    {

        _collisions[col.collider] = col;
    }

    void OnCollisionExit(Collision coll)
    {
        _collisions.Remove(coll.collider);
        //Debug.Log("remove colission to:" + _collisions.Count);
    }

    private void crushTest(Collision coll)
    {
        // first, see if the player is touching anything else.
        bool rebuild = false;
        Collision[] collision_array = new Collision[_collisions.Count];
        _collisions.Values.CopyTo(collision_array, 0);

        if (collision_array.GetLength(0) == 0) return;

        // cycle through the other collisions and detect what the normal is.
        // if the difference between the normals is more than 90 degrees, the player has been crushed.
        Vector3 new_normal = coll.contacts[0].normal;

        foreach (Collision existing_coll in collision_array)
        {
            Vector3 existing_normal = existing_coll.contacts[0].normal;
            if (existing_coll.collider == null)
            {
                rebuild = true;
            }
            float normal_angle = Vector3.Angle(new_normal, existing_normal);
            if (normal_angle > angleTolerance && (isInMask(coll.gameObject) || isInMask(existing_coll.gameObject)))
            {
                //Debug.Log("crush");
                _entity.raiseDeath();
                return;
            }
        }
        if(rebuild)
        {
            fixDictionarry();
        }
    }

    private void fixDictionarry()
    {
        Collider[] colliders = new Collider[_collisions.Count];
        _collisions.Keys.CopyTo(colliders, 0);
        Dictionary<Collider, Collision> newDictionary = new Dictionary<Collider, Collision>();
        foreach(Collider col in colliders)
        {
            if(col!=null)
            {
                newDictionary.Add(col, _collisions[col]);
            }
        }
        _collisions = newDictionary;
    }


    private bool isInMask(GameObject gobj)
    {
        return (crushingLayers.value & 1<<gobj.layer)==1<<gobj.layer;
    }
    //void FixedUpdate()
    //{
    //    collisions.Clear();
    //}

    //void OnCollisionStay(Collision coll)
    //{
    //    collisions.Add(coll);
    //}

    //void OnDrawGizmos()
    //{
    //    if (collisions == null) return;

    //    foreach (Collision coll in collisions)
    //    {
    //        Gizmos.color = Color.white;
    //        Collider collider = coll.collider;
    //        Gizmos.DrawCube(collider.bounds.center, collider.bounds.size);

    //        Gizmos.color = Color.red;
    //        ContactPoint[] points = coll.contacts;
    //        foreach (ContactPoint point in points)
    //        {
    //            Gizmos.DrawCube(point.point, Vector3.one * 10);
    //            Gizmos.DrawRay(point.point, point.normal * 25);
    //        }
    //    }
    //}

    public Collision[] Collisions
    {
        get
        {
            Collision[] collision_array = new Collision[_collisions.Count];
            _collisions.Values.CopyTo(collision_array,0);
            return collision_array;
        }
    }
}
