﻿using UnityEngine;
using System.Collections;
using System;

/// <summary>
/// the main controller for the player character. 
/// this class handels inputs as well as tracks the players' condition.
/// </summary>
public class PlayerController : Entity  {

    
    public delegate void PlayerInteract();
    /// <summary>
    /// this event fires when the p[layer clicks the interact button
    /// </summary>
    public static event PlayerInteract playerInteraction;

    /// <summary>
    /// 
    /// </summary>
    public static PlayerController instance;



    /// <summary>
    /// the current movment module.
    /// </summary>
	[SerializeField]
	private PhisicesMover thruster;
    /// <summary>
    /// the current weapon modules.
    /// </summary>
    [SerializeField]
    private Weapon[] weapon;



    //public GameObject actionPopup;

	#region key binding
	public KeyCode rightKey;
	public KeyCode leftKey;
	public KeyCode upKey;
    public KeyCode fire;
    public KeyCode reload;
    public KeyCode interact;
	#endregion
    /// <summary>
    /// the command mask relaying the players movments to the thruster
    /// </summary>
	private CommandMask thrustCommands;
    /// <summary>
    /// the command mask relaying commands to the weapon.
    /// </summary>
    private CommandMask weaponCommands;
    /// <summary>
    /// the players robot animation component.
    /// </summary>
    private Animator robotAnimator;

    public Transform cameraTarget;


    protected override void  Awake()
    {
        base.Awake();
        if (instance == null)
        {
            DontDestroyOnLoad(gameObject);
            instance = this;

        }
        else
        {

            gameObject.SetActive(false);
            instance.transform.SetParent(transform.parent);
            instance.transportPlayer(transform.position, false);
            instance.enabled = true;
            Destroy(gameObject);
            AimController thisAim = GetComponentInChildren<AimController>();
            AimController instanceAim = instance.GetComponentInChildren<AimController>();
            if(thisAim == null)
            {
                instanceAim.viewCamera = Camera.main;
            }
            else
            {
                instanceAim.viewCamera = thisAim.viewCamera;
            }
        }

    }

	// Use this for initialization
	void Start () 
	{
        robotAnimator = GetComponent<Animator>();
		thrustCommands = new CommandMask();
        weaponCommands = new CommandMask();
        getParts();
        _alive = true;
        registerTracker();

	}
    /// <summary>
    /// finds the controlled parts' scripts.
    /// </summary>
    public void getParts()
    {
        
        thruster = GetComponentInChildren<PhisicesMover>();
        weapon = GetComponentsInChildren<Weapon>();
    }
	/// <summary>
    /// registers the HP tracker to the <seealso cref="GameAttributeManager"/> of the player.
    /// </summary>
    public void registerTracker()
    {
        GameAttributeManager manager = GetComponent<GameAttributeManager>();
        if(manager!=null)
        {
            manager.OnAttributeMofified += trackHp;
        }
    }
    
    void Update()
    {
        //if (weapon == null)
        //{
        //    weapon = GetComponentsInChildren<Weapon>();
        //}
        if (_alive && !GameManager.instance._lowPause)
        {
            if (Time.timeScale > 0f)//only collects input if the game isnt paused
            {
                weaponCommands.reset(); //only weapon commands are collected during Update
                weaponCommands.addCommand(UserCommands.UPDATE_CALL);
                if (Input.GetKey(fire))
                {
                    weaponCommands.addCommand(UserCommands.FIRE);
                }
                if (Input.GetKeyDown(reload))
                {
                    weaponCommands.addCommand(UserCommands.RELOAD);
                }
                if (Input.GetKeyDown(interact))
                {
                    if (playerInteraction != null)
                    {
                        playerInteraction.Invoke();
                    }
                }
                if (weapon != null)
                {
                    for (int i = 0; i < weapon.Length; i++)
                    {
                        weapon[i].getCommands(weaponCommands);
                    }
                }
            }
        }
        
    }

   

	void FixedUpdate()
	{
        //if (thruster==null)
        //{
        //    thruster = GetComponentInChildren<PhisicesMover>();
        //}
        if (_alive  && !GameManager.instance._lowPause)
        {
            thrustCommands.reset(); //collect movment commands
            thrustCommands.addCommand(UserCommands.FIXED_UPDATE_CALL);
            if (Input.GetKey(rightKey))
            {
                thrustCommands.addCommand(UserCommands.RIGHT_THRUST);

            }
            if (Input.GetKey(leftKey))
            {
                thrustCommands.addCommand(UserCommands.LEFT_THRUST);
            }
            if (Input.GetKey(upKey))
            {
                thrustCommands.addCommand(UserCommands.JUMP);
            }
            //Debug.Log (thrustCommands);
            if (thruster != null)
            {
                thruster.getCommands(thrustCommands);
            }
        }
	}
    

    protected override IEnumerator death()
    {
        
            invokeDeath();
            GameManager.instance._lowPause = true;
            robotAnimator.SetBool("MirrorZ", !thruster.isFacingRight);
            robotAnimator.SetBool("Alive", false);
            yield return new WaitForSeconds(6f);
            //StartCoroutine(waitOnAnimation(robotAnimator));
            //CallDeath = true;
            //Vector3 lastPrinterPosition = LevelManager.ActiveLevel.ActivePrinter.transform.position;
            //transform.position = new Vector3(lastPrinterPosition.x,lastPrinterPosition.y,0f);
            transportPlayer(LevelManager.ActiveLevel.ActivePrinter.output.position, false);
            robotAnimator.SetBool("Alive", true);
            GameAttributeManager attributes = GetComponent<GameAttributeManager>();
            if (attributes != null)
            {
                attributes.setAttributeValue(GameAttributeCodes.CURRENT_HP, attributes.getAttributeiFloat(GameAttributeCodes.MAX_HP));
            }
            //yield return new WaitForSeconds(1f);
            _alive = true;
            GameManager.instance._lowPause = false;
            // CallDeath = true;
           // yield return null;
        
    }

    public void transportPlayer(Vector3 destination, bool keepVelocity)
    {
        transform.position = new Vector3 (destination.x,destination.y);
        if(!keepVelocity)
        {
            Rigidbody rbody = instance.GetComponent<Rigidbody>();
            rbody.velocity = Vector3.zero;
        }
    }

    //void OnCollisionStay(Collision coll)
    //{
    //    // first, see if the player is touching anything else.
    //    Collision[] collisions = collisionManager.Collisions;

    //    if (collisions.GetLength(0) == 0) return;

    //    // cycle through the other collisions and detect what the normal is.
    //    // if the difference between the normals is more than 90 degrees, the player has been crushed.
    //    Vector3 new_normal = coll.contacts[0].normal;

    //    foreach (Collision existing_coll in collisions)
    //    {
    //        Vector3 existing_normal = existing_coll.contacts[0].normal;

    //        float normal_angle = Vector3.Angle(new_normal, existing_normal);
    //        if (normal_angle > 135)
    //            raiseDeath();
    //    }
    //}
}
